/**
 * @format
 */

import 'react-native';

import * as scryfallApi from '@apis/scryfallApi';
require('jest-fetch-mock').enableMocks();

beforeEach(() => {
  fetchMock.resetMocks();
});

it('jest-fetch-mock works', async () => {
  fetchMock.mockResponseOnce('{ "id": 1 }', {
    status: 200,
    headers: {'content-type': 'application/json'},
  });

  const result = await scryfallApi.getAsync('sets');
  expect(result.id).toEqual(1);
});
