import 'react-native-gesture-handler'; //required by react-navigation
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {configureFonts,DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {CColors} from '@src/colors';
import {store} from './store';
import {Provider} from 'react-redux';
import {navigationRef} from '@src/RootNavigation';
import {enableScreens} from 'react-native-screens';

//https://github.com/software-mansion/react-native-screens
enableScreens();

const fontConfig = {
    default: {
      regular: {
        fontFamily: 'NunitoSans-Light',
        fontWeight: 'normal',
      },
      medium: {
        fontFamily: 'NunitoSans-Light',
        fontWeight: 'normal',
      },
      light: {
        fontFamily: 'NunitoSans-Light',
        fontWeight: 'normal',
      },
      thin: {
        fontFamily: 'NunitoSans-Light',
        fontWeight: 'normal',
      },
    },
  };

const theme = {
  ...DefaultTheme,
  //https://callstack.github.io/react-native-paper/theming.html
  colors: {
    ...DefaultTheme.colors,
    background: CColors.background,
    primary: CColors.primary,
    text: CColors.text.default,
  },
  fonts:configureFonts(fontConfig)

};

const Root = (props) => (
  <Provider store={store}>
    <PaperProvider theme={theme}>
      <NavigationContainer theme={theme} ref={navigationRef}>
        <App {...props} />
      </NavigationContainer>
    </PaperProvider>
  </Provider>
);

AppRegistry.registerComponent(appName, () => Root);
