module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        alias: {
          '@screens': './src/_screens',
          '@apis': './src/apis',
          '@components': './src/_components',
          '@containers': './src/_containers',
          '@src': './src',
        },
      },
    ],
  ],
};
