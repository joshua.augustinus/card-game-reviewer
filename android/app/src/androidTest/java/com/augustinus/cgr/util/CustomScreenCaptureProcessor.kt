package com.augustinus.cgr.util

import androidx.test.runner.screenshot.BasicScreenCaptureProcessor


class CustomScreenCaptureProcessor : BasicScreenCaptureProcessor() {
    public override fun getFilename(prefix: String): String {
        return prefix
    }
}