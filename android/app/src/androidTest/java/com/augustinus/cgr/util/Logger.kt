package com.augustinus.cgr.util

import android.util.Log

object Logger{
    fun log(message: String?) {
        Log.d("Espresso", message)
    }
}