package com.augustinus.cgr.util

import android.graphics.Bitmap
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.screenshot.Screenshot
import androidx.test.uiautomator.*
import org.junit.Assert
import java.io.IOException


object UiAutoHelper{
    //Value of 20+ doesn't work on VIVO
    val SWIPE_STEPS = 10

    fun closeApps(){
        try {

            val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());


            if(isVivo())
            {

                uiDevice.pressHome()
                tapText("Clock")
            }else{
                //Close apps

                uiDevice.pressRecentApps();
                waitForText("Close all")


                val clearAll = uiDevice.findObject(By.text("Close all"));
                if(clearAll!=null)
                    clearAll.click();
            }




        }catch (e: Exception){

        }

    }

    fun isVivo():Boolean{
     return false
    }


    fun swipeLeft(){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val maxY = uiDevice.displayHeight;
        val maxX = uiDevice.displayWidth;
        val centerX = maxX/2;
        val centerY = maxY/2;
        val endX = 0;
        val endY = centerY;
        uiDevice.swipe(centerX, centerY, endX, endY, SWIPE_STEPS)
    }

    fun swipeRight(){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val maxY = uiDevice.displayHeight;
        val maxX = uiDevice.displayWidth;
        val centerX = maxX/2;
        val centerY = maxY/2;
        val endX = maxX;
        val endY = centerY;
        uiDevice.swipe(centerX, centerY, endX, endY, SWIPE_STEPS)
    }


    fun waitForText(text: String){
        val waitTime:Long = 10000;
        waitForText(text, waitTime)
    }

    fun waitForTextLong(text: String){
        val waitTime:Long = 1000 * 60;
        waitForText(text, waitTime)
    }

    fun waitForText(text: String, waitTime: Long){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val result = uiDevice.wait(Until.hasObject(By.textContains(text)), waitTime);
        if(!result)
            Assert.fail("Couldn't find text: $text");

    }

    fun doesTextExist(text: String) : Boolean {
        val waitTime:Long = 1000;
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        val result = uiDevice.wait(Until.hasObject(By.textContains(text)), waitTime);
        if(!result)
            return false;
        else
            return true;
    }

    /**
     * This screenshot method will save to Pictures folder
     */
    fun screenshot(name: String) {

        val processor = CustomScreenCaptureProcessor()
        val capture = Screenshot.capture()
        capture.format = Bitmap.CompressFormat.PNG
        capture.name = name
        try {
            processor.process(capture)
            Logger.log("Took pictures capture: $name")
        } catch (ex: IOException) {
            Logger.log(ex.message)
        }

    }

    /**
     * Will only work with full text match
     */
    fun tapText(text: String){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val button = uiDevice.findObject(UiSelector().text(text))
        button.click()
    }

    /**
     * Find element based on accessibilityLabel
     */
    fun tapAccessLabel(text: String){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val button = uiDevice.findObject(UiSelector().descriptionContains(text))
        button.click()
    }

    /**
     * Finds an input element with accessibilityLabel and sets the text
     */
    fun setText(accessibilityLabel: String, text: String){
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val element = uiDevice.findObject(UiSelector().descriptionContains(accessibilityLabel))
        element.text = text;

    }

}