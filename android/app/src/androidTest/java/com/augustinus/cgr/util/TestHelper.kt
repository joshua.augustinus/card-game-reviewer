package com.augustinus.cgr.util

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withTagValue
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString


object TestHelper {
    /**
     * Tap a testID
     */
    fun tapId(tagValue: String){
        val viewWithTagVI = onView(withTagValue(`is`(tagValue as Any)))
        viewWithTagVI.perform(click())
    }

    /**
     * Scrolls to element with text; Partial match allowed
     */
    fun scrollToText(text:String){
        onView(withText(containsString(text))).perform(scrollTo())
    }

}