package com.augustinus.cgr

import android.Manifest
import android.util.Log
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import com.augustinus.cgr.util.TestHelper.scrollToText
import com.augustinus.cgr.util.TestHelper.tapId
import com.augustinus.cgr.util.UiAutoHelper
import com.augustinus.cgr.util.UiAutoHelper.closeApps
import com.augustinus.cgr.util.UiAutoHelper.doesTextExist
import com.augustinus.cgr.util.UiAutoHelper.screenshot
import com.augustinus.cgr.util.UiAutoHelper.setText
import com.augustinus.cgr.util.UiAutoHelper.swipeLeft
import com.augustinus.cgr.util.UiAutoHelper.swipeRight
import com.augustinus.cgr.util.UiAutoHelper.tapAccessLabel
import com.augustinus.cgr.util.UiAutoHelper.tapText
import com.augustinus.cgr.util.UiAutoHelper.waitForText
import com.augustinus.cgr.util.UiAutoHelper.waitForTextLong
import com.cgr.MainActivity
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4



@LargeTest
class ComplexTests {
    @Rule
    @JvmField
    var permissionRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)

    @Rule
    @JvmField
    var permissionWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE)


    private var activityScenario: ActivityScenario<MainActivity>? = null
    lateinit var mainActivity: MainActivity
    @Before
    fun beforeEach() {
        activityScenario = ActivityScenario.launch(MainActivity::class.java)


        activityScenario!!.onActivity(ActivityScenario.ActivityAction { activity: MainActivity ->
            mainActivity = activity;

        })

        if(doesTextExist("Reset"))
            tapText("Reset")


        waitForTextLong("Who is this app for?")
    }

    @After
    fun cleanup() {

    }

    /**
     * Has Home Screens
     */
    @Test
    fun hasHomeScreens() {

        screenshot("Home_1")

        swipeLeft()
        waitForText("Marvel & Scryfall API")
        screenshot("Home_2")

        swipeLeft()
        waitForText("Why Scryfall API?")
        screenshot("Home_3")

        swipeLeft()
        waitForText("Why Marvel API?")
        screenshot("Home_4")

        swipeLeft()
        waitForText("What is inside this app?")
        screenshot("Home_5")

        tapText("Start")
        waitForText("Introduction")

    }

    @Test
    fun sideMenuWorks() {

        tapText("Start")
        waitForText("Introduction")
        tapAccessLabel("Menu icon")
        waitForText("Typography")

    }

    @Test
    fun bottomNavWorks() {

        tapText("Start")
        waitForText("Introduction")
        tapAccessLabel("Menu icon")
        waitForText("Typography")
        tapText("BottomNavigator")
        tapText("react-native-paper Bottom Navigator")
        waitForText("John Doe")
        screenshot("react-native-paper account")
        tapAccessLabel("random tab")
        //Not sure what to check here because it's a webview
        waitForText("Random")
        Thread.sleep(3000)
        screenshot("react-native-paper random")
        tapAccessLabel("stocks Tab")
        waitForText("Stocks")
        screenshot("react-native-paper stocks")
        tapAccessLabel("home Tab")
        waitForText("Custom Bottom Navigator")
        screenshot("react-native-paper home")

    }

    @Test
    fun marvelScreenWorks() {

        tapText("Start")
        waitForText("Introduction")
        tapAccessLabel("Menu icon")
        waitForText("Typography")
        scrollToText("Selectable List Item")
        tapText("- Selectable List Item")
        waitForText("See Example")
        tapText("See Example")
        waitForText("3-D Man")
        screenshot("Marvel Search")
        setText("Search Filter", "cap")
        waitForText("Captain America")
        tapText("Captain America")
        waitForText("Captain America (2018)")
        screenshot("Character Profile")
    }

}