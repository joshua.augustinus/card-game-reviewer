#https://medium.com/idopterlabs/working-with-environment-variables-in-appcenter-aba44716eda0
#> Means output to file
#>> Means append to file
cd ${APPCENTER_SOURCE_DIRECTORY}
echo "MARVEL_PUBLIC_KEY=${MARVEL_PUBLIC_KEY}" > .env
echo "MARVEL_PRIVATE_KEY=${MARVEL_PRIVATE_KEY}" >> .env
echo "ENVIRONMENT=${ENVIRONMENT}" >> .env