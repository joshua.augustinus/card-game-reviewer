import {useWindowDimensions} from 'react-native';
import {getFontSizes} from './fontSizes';

const SMALL_WIDTH = 500;

/**
 * ALlows us to get screen dimensions but also tells if the screen is small
 */
const useScreenInfo = () => {
  const windowInfo = useWindowDimensions();
  const isSmallScreen = windowInfo.width < SMALL_WIDTH;
  return {
    ...windowInfo,
    isSmallScreen: isSmallScreen,
    fontSizes: getFontSizes(isSmallScreen),
  };
};

export {useScreenInfo};
