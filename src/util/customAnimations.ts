/**
 * https://github.com/oblador/react-native-animatable/tree/master/definitions
 */

export const bounceOutLeft = {
  0: {
    translateX: 0,
  },
  0.2: {
    translateX: -10,
  },
  0.4: {
    translateX: 20,
  },
  0.45: {
    translateX: 20,
  },
  1: {
    translateX: -800,
  },
};
