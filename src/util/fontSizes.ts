export const smallScreen = {
  normal: 14,
  paragraph: 14,
  subHeading: 15,
  title: 26,
  small: 12,
};

export const mediumScreen = {
  normal: 16,
  paragraph: 16,
  subHeading: 18,
  title: 30,
  small: 12,
};

export function getFontSizes(isSmall: boolean) {
  return isSmall ? smallScreen : mediumScreen;
}

/**
 * Use useScreenInfo() hook to get fontSizes
 */
