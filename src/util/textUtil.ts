export const normaliseText = (inputText: string) => {
  if (inputText === null) return '';

  var currentLetter: string;
  var result = '';

  for (let i = 0; i < inputText.length; i++) {
    currentLetter = inputText[i];

    if (i === 0) result += currentLetter;
    else if (isCapital(currentLetter)) {
      if (result[i - 1] !== ' ') result += ' ';
      result += currentLetter;
    } else {
      result += currentLetter;
    }
  }
  return result;
};

const isCapital = (letter: string) => {
  return letter.toUpperCase() === letter;
};
