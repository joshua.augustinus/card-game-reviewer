const elevations = {
  LOW: 3,
  MEDIUM: 5,
  HIGH: 7,
};

export {elevations};
