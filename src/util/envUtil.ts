import Config from 'react-native-config';
export const isTestMode = Config.ENVIRONMENT === 'TEST';
