import {CActivityIndicator} from '@components/CActivityIndicator';
import React, {useState} from 'react';

import {WebView} from 'react-native-webview';
import {WebViewProgressEvent} from 'react-native-webview/lib/WebViewTypes';

const PROGRESS_THRESHOLD = 0.7;

interface Props {
  url: string;
}
const CWebView = (props: Props) => {
  const [isLoading, setIsLoading] = useState(true);

  const onLoad = (loadEvent: WebViewProgressEvent) => {
    const progress = loadEvent.nativeEvent.progress;
    if (isLoading && progress >= PROGRESS_THRESHOLD) {
      setIsLoading(false);
    } else if (!isLoading && progress <= PROGRESS_THRESHOLD) {
      setIsLoading(true);
    }
  };

  return (
    <>
      <WebView source={{uri: props.url}} onLoadProgress={onLoad} />

      {isLoading && <CActivityIndicator type="absolute" />}
    </>
  );
};

export {CWebView};
