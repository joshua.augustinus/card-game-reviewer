import {useNavigation} from '@react-navigation/native';
import React, {ReactNode, useState} from 'react';
import {View} from 'react-native';

interface Props {
  children: ReactNode;
  style?: any;
}

/**
 * This will unmount children components when it loses focus
 */
const ForgetfulView = (props: Props) => {
  const navigation = useNavigation();
  const [isFocused, setIsFocused] = useState(true);

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      // do something
      setIsFocused(false);
    });

    return unsubscribe;
  }, [navigation]);

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // do something
      setIsFocused(true);
    });

    return unsubscribe;
  }, [navigation]);

  if (isFocused) return <View style={props.style}>{props.children}</View>;
  else return null;
};

export {ForgetfulView};
