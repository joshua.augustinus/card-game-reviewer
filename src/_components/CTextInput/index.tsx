import React from 'react';
import {Keyboard} from 'react-native';
import {TextInput} from 'react-native-paper';

/**
 * Set clearable=true to allow for clearing of text input
 */
interface Props {
  accessibilityLabel?: string;
  value: string;
  label: string;
  onChangeText: (text: string) => void;
  style?: any;
  clearable?: boolean;
}

const CTextInput = (props: Props) => {
  const rightIcon = props.clearable ? (
    <TextInput.Icon
      name="close"
      onPress={() => {
        props.onChangeText('');
        Keyboard.dismiss();
      }}
    />
  ) : undefined;

  return (
    <TextInput
      right={rightIcon}
      style={props.style}
      accessibilityLabel={props.accessibilityLabel}
      value={props.value}
      label={props.label}
      onChangeText={props.onChangeText}
    />
  );
};

export {CTextInput};
