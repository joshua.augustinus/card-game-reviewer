import React, {ReactNode, useEffect, useRef} from 'react';

import {Alert, Animated, ViewStyle} from 'react-native';

interface Props {
  style?: ViewStyle;
  children: ReactNode;
  toValue: number;
  delay?: number;
  startValue?: number;
  axis?: 'x' | 'y';
  duration?: number;
}

const DEFAULT_DURATION = 800;

const SlidingView: React.FunctionComponent<Props> = (props) => {
  const dynamicValue = useRef(new Animated.Value(props.startValue)).current;

  let transformArray = [];
  if (props.axis === 'x') {
    transformArray.push({translateX: dynamicValue});
  } else {
    transformArray.push({translateY: dynamicValue});
  }

  useEffect(() => {
    const duration = props.duration ? props.duration : DEFAULT_DURATION;
    Animated.timing(dynamicValue, {
      delay: props.delay,
      toValue: props.toValue,
      duration: duration,
      useNativeDriver: true,
    }).start();
  }, [props.toValue]);

  return (
    <Animated.View style={{...props.style, transform: transformArray}}>
      {props.children}
    </Animated.View>
  );
};

SlidingView.defaultProps = {
  startValue: 0,
  axis: 'x',
};

export {SlidingView};
