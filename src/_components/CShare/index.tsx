import React from 'react';
import {CColors} from '@src/colors';
import {TextType} from '@src/types';
import {Button} from 'react-native-paper';
import {useScreenInfo} from '@src/util/customHooks';
import Share from 'react-native-share';

interface Props {
  style?: any;
  textType: TextType;
  children: any;
  subject: string;
  urls: string[];
}

const CShare = (props: Props) => {
  const screenInfo = useScreenInfo();
  const fontSize = screenInfo.fontSizes[props.textType];
  let textColor = CColors.button.primary;

  /**
   * For some reason only the first url gets shared
   */
  const options = {
    subject: props.subject,
    urls: props.urls,
  };

  const clickHandler = () => {
    Share.open(options)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        err && console.log(err);
      });
  };

  return (
    <Button
      icon="share-variant"
      style={props.style}
      labelStyle={{fontSize: fontSize, fontFamily: 'Roboto'}}
      color={textColor}
      uppercase={false}
      onPress={clickHandler}>
      {props.children}
    </Button>
  );
};

export {CShare};
