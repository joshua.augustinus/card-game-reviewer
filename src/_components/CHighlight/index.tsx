import React, {ReactNode} from 'react';
import {CText} from '@components/CText';

interface Props {
  children: ReactNode;
}

/**
 * Highlights text
 */
const CHighlight = (props: Props) => {
  return (
    <CText type="normal" color="accent">
      {props.children}
    </CText>
  );
};

export {CHighlight};
