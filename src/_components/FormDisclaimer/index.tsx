import React from 'react';
import * as Animatable from 'react-native-animatable';
import {Text} from 'react-native-paper';
import {CCard} from '../../_components/CCard';

const FormDisclaimer = () => {
  return (
    <Animatable.View animation="bounceInRight" delay={500}>
      <CCard danger>
        <Text>
          This form is just for demonstration and is not functional. Press the
          back button on your device to return to the previous screen. Or press
          the menu icon in the top left and navigate to the next screen.
        </Text>
      </CCard>
    </Animatable.View>
  );
};

export {FormDisclaimer};
