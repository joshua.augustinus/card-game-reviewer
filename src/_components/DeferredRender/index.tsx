import {SlidingView} from '@components/SlidingView';
import React, {ReactNode, useEffect, useState} from 'react';
import {View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

interface Props {
  children: ReactNode;
  noScrollView?: boolean;
  delay?: number;
}
/**
 * To have the menu slide animation close smoothly we render content after first mount
 */
const DeferredRender = (props: Props) => {
  const [renderContent, setRenderContent] = useState(false);
  const delay = props.delay ? props.delay : 0;
  useEffect(() => {
    setRenderContent(true);
  }, []);

  if (!renderContent) return <View style={{flex: 1}}></View>;

  if (props.noScrollView) {
    return (
      <SlidingView
        delay={delay}
        startValue={-800}
        toValue={0}
        duration={500}
        style={{flex: 1}}>
        {props.children}
      </SlidingView>
    );
  } else {
    return (
      <SlidingView delay={delay} startValue={-800} toValue={0} duration={500}>
        <ScrollView>
          {props.children}
          <View style={{height: 10}}></View>
        </ScrollView>
      </SlidingView>
    );
  }
};

export {DeferredRender};
