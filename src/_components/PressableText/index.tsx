import React from 'react';
import {CColors} from '@src/colors';
import {TextType} from '@src/types';
import {Button} from 'react-native-paper';
import {useScreenInfo} from '@src/util/customHooks';

interface Props {
  style?: any;
  textType: TextType;
  children: any;
  onPress: (data: any) => void;
  data: any;
}

const PressableText = React.memo((props: Props) => {
  const screenInfo = useScreenInfo();
  const fontSize = screenInfo.fontSizes[props.textType];
  let textColor = CColors.button.primary;

  return (
    <Button
      style={props.style}
      labelStyle={{fontSize: fontSize}}
      color={textColor}
      uppercase={false}
      onPress={() => props.onPress(props.data)}>
      {props.children}
    </Button>
  );
});

export {PressableText};
