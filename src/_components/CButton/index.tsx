import {Button} from 'react-native-elements';
import React from 'react';
import {CColors} from '@src/colors';
import {ButtonType} from '@src/types';

interface Props {
  title: string;
  style?: any;
  type?: ButtonType;
  onPress: () => void;
  small?: boolean;
}

const CButton = (props: Props) => {
  let backgroundColor = CColors.button.primary;
  let textColor = CColors.buttonText.primary;
  if (props.type) {
    backgroundColor = CColors.button[props.type];
    textColor = CColors.buttonText[props.type];
  }

  const fontSize = props.small ? 15 : 25;
  return (
    <Button
      raised
      title={props.title}
      buttonStyle={{
        borderRadius: 5,
        backgroundColor: backgroundColor,
        paddingVertical: 15,
      }}
      titleStyle={{
        color: textColor,
        fontSize: fontSize,
        fontWeight: 'normal',
      }}
      containerStyle={{
        ...props.style,
      }}
      onPress={props.onPress}
    />
  );
};

export {CButton};
