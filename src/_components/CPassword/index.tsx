import React, {useState} from 'react';

import {TextInput} from 'react-native-paper';

interface Props {
  value: string;
  onChangeText: (value: string) => void;
  style?: any;
  label: string;
}

const CPassword = (props: Props) => {
  const [passwordVisibility, setPasswordVisibility] = useState(false);
  const passwordIcon = passwordVisibility ? 'eye-off' : 'eye';
  return (
    <TextInput
      style={props.style}
      secureTextEntry={!passwordVisibility}
      autoCapitalize="none"
      label={props.label}
      value={props.value}
      onChangeText={props.onChangeText}
      right={
        <TextInput.Icon
          style={{opacity: 0.5}}
          name={passwordIcon}
          onPress={() => {
            setPasswordVisibility(!passwordVisibility);
          }}
        />
      }
    />
  );
};

export {CPassword};
