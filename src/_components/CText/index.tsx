import React, {ReactNode} from 'react';
import {useScreenInfo} from '@src/util/customHooks';
import {Text} from 'react-native-paper';
import {ColorType, TextType} from '@src/types';
import {CColors} from '@src/colors';

interface Props {
  style?: any;
  children: ReactNode;
  type: TextType;
  color?: ColorType;
  fontFamily?: string;
}

/**
 * This class will choose font size depending on type and screen size
 * To have font size with fixed font size use Text from react-native-paper instead
 */
const CText = React.memo((props: Props) => {
  const screenInfo = useScreenInfo();
  const fontSizes = screenInfo.fontSizes;

  let fontSize = fontSizes[props.type];
  const colorType = props.color ? props.color : 'default';
  let customStyle = {
    fontSize: fontSize,
    color: CColors.text[colorType],
    ...styles[props.type],
  };

  if (props.fontFamily) {
    customStyle = {
      ...customStyle,
      fontFamily: props.fontFamily,
    };
  }

  return <Text style={{...props.style, ...customStyle}}>{props.children}</Text>;
});

export {CText};

const styles = {
  paragraph: {
    marginBottom: 12,
  },
  normal: {},
  title: {fontFamily: 'Satisfy-Regular'},
  subHeading: {},
  small: {},
};
