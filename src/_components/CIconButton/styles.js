import {CColors} from '@src/colors';

import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  iconButton: {
    backgroundColor: CColors.button.primary,
  },
  flatLeft: {
    borderBottomLeftRadius: 0,
    borderTopLeftRadius: 0,
  },
});

export default styles;
