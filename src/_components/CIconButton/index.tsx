import React from 'react';
import {IconButton} from 'react-native-paper';
import {CColors} from '@src/colors';
import styles from './styles';

interface Props {
  onPress: () => void;
  /**
   * Material community icon name
   */
  iconName: string;
  flatLeft?: boolean;
}

/**
 * The actual screen
 */
const CIconButton = (props: Props) => {
  let customStyle = {};
  if (props.flatLeft) {
    customStyle = styles.flatLeft;
  }

  return (
    <IconButton
      style={{...styles.iconButton, ...customStyle}}
      icon={props.iconName}
      color={CColors.buttonText.primary}
      size={30}
      onPress={props.onPress}
    />
  );
};
export {CIconButton};
