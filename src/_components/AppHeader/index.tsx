import React from 'react';
import {Text, useTheme} from 'react-native-paper';
import styles from './styles';
import {Pressable, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '@src/types';
import {IconButton} from 'react-native-paper';
import {toggleDrawer} from '@src/RootNavigation';
import {deleteState} from '@src/reducers';

interface Props {
  showMenuToggle: boolean;
}

const AppHeader = (props: Props) => {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const title = useSelector((state: RootState) => state.appHeaderTitle);

  /**
   * Used by E2E Tests to reset
   */
  const tapHiddenHeader = () => {
    dispatch(deleteState());
  };

  return (
    <View style={{...styles.container, backgroundColor: colors.primary}}>
      {props.showMenuToggle && (
        <IconButton
          accessibilityLabel="Menu icon"
          icon="menu"
          color="white"
          size={20}
          onPress={() => toggleDrawer()}
        />
      )}

      <View>
        <Pressable onPress={tapHiddenHeader}>
          <Text style={styles.hiddenHeader}>Reset</Text>
        </Pressable>
      </View>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>{title}</Text>
      </View>
    </View>
  );
};

export default AppHeader;
