import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 60,
    paddingHorizontal: 0,
    paddingRight: 10,
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerContainer: {flex: 1, alignItems: 'flex-end'},
  headerText: {
    color: 'white',
  },
  hiddenHeader:{
    fontSize:1
  },
  logo: {
    height: '100%',
    width: 100,
    marginLeft: 5,
  },
  svgWrapper: {
    aspectRatio: 1,
    flex: 1,
  },
});

export default styles;
