import React from 'react';
import {CColors} from '@src/colors';
import {ButtonType, TextType} from '@src/types';
import {Button} from 'react-native-paper';
import {useScreenInfo} from '@src/util/customHooks';

interface Props {
  style?: any;
  type?: ButtonType;
  onPress: () => void;
  textType: TextType;
  children: any;
}

const CPaperButton = (props: Props) => {
  const screenInfo = useScreenInfo();
  const fontSize = screenInfo.fontSizes[props.textType];
  let backgroundColor = CColors.button.primary;
  let textColor = CColors.buttonText.primary;
  if (props.type) {
    backgroundColor = CColors.button[props.type];
    textColor = CColors.buttonText[props.type];
  }

  return (
    <Button
      style={props.style}
      labelStyle={{fontSize: fontSize, fontFamily: 'Roboto'}}
      color={backgroundColor}
      uppercase={false}
      mode="contained"
      onPress={props.onPress}>
      {props.children}
    </Button>
  );
};

export {CPaperButton};
