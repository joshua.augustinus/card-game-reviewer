import React from 'react';

import {View} from 'react-native';
import styles from './styles';
import {ActivityIndicator} from 'react-native-paper';
import {CColors} from '@src/colors';
import {useScreenInfo} from '@src/util/customHooks';
import {ActivityIndicatorType} from '@src/types';

interface Props {
  type?: ActivityIndicatorType;
}

/**
 * The actual screen
 */
const CActivityIndicator = (props: Props) => {
  if (props.type === 'absolute') {
    return (
      <ActivityIndicator
        style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          marginLeft: 'auto',
          marginRight: 'auto',
          left: 0,
          right: 0,
        }}
        size="large"
        color={CColors.button.primary}
      />
    );
  } else if (props.type === 'small') {
    return (
      <ActivityIndicator
        style={{
          position: 'absolute',
          bottom: 10,
          left: 0,
          right: 0,
        }}
        size="small"
        color={CColors.button.primary}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color={CColors.button.primary} />
      </View>
    );
  }
};

export {CActivityIndicator};
