import {StyleSheet} from 'react-native';
import {elevations} from '@src/util/elevations';
import {CColors} from '@src/colors';

const styles = StyleSheet.create({
  card: {
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    zIndex: 0,
    elevation: elevations.LOW,
  },
  danger: {
    borderColor: CColors.danger,
    borderWidth: 1,
  },
});

export default styles;
