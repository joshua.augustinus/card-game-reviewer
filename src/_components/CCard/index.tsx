import {CColors} from '@src/colors';
import React, {ReactNode} from 'react';
import {Card} from 'react-native-paper';
import styles from './styles';

interface Props {
  children: ReactNode;
  style?: any;
  fill?: boolean;
  danger?: boolean;
  inner?: boolean;
}

const CCard = (props: Props) => {
  let customStyle: React.CSSProperties = {};
  if (props.danger) {
    customStyle = styles.danger;
  }
  if (props.inner) {
    customStyle = {
      ...customStyle,
      marginLeft: 0,
      marginRight: 0,
      marginBottom: 15,
      marginTop: 15,
    };
  }

  if (props.fill) {
    return (
      <Card
        style={{
          ...styles.card,

          ...props.style,
          ...customStyle,
          flex: 1,
          marginBottom: styles.card.marginTop,
        }}>
        {props.children}
      </Card>
    );
  } else {
    return (
      <Card
        style={{
          ...styles.card,
          ...props.style,
          ...customStyle,
        }}>
        <Card.Content>{props.children}</Card.Content>
      </Card>
    );
  }
};

export {CCard};
