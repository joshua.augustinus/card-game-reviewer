import React from 'react';

import {CShare} from '@components/CShare';

const URLS = ['https://jauggy.atlassian.net/wiki/spaces/RNDWS/overview'];

const ShareConfluence = () => {
  return (
    <CShare
      urls={URLS}
      subject="Documentation for React Native Demo"
      textType="normal">
      Confluence Documentation
    </CShare>
  );
};

export {ShareConfluence};
