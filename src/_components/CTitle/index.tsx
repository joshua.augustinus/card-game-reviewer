import React, {ReactNode} from 'react';
import {View} from 'react-native';
import {Title} from 'react-native-paper';
import {CCard} from '../../_components/CCard';
import {StyleSheet} from 'react-native';
import {CText} from '@components/CText';

interface Props {
  children: ReactNode;
}

const CTitle = (props: Props) => {
  return (
    <CText type="subHeading" style={styles.title}>
      {props.children}
    </CText>
  );
};

export {CTitle};

const styles = StyleSheet.create({
  title: {
    alignSelf: 'center',
    marginBottom: 10,
  },
});

export default styles;
