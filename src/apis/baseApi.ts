/**
 * Fetches data from API
 * @param route Do not include trailing slash
 * @param baseUrl Do not include slash
 */
export function getAsync(route: string, baseUrl: string) {
  let url = combineBaseUrlAndRoute(route, baseUrl);
  let accessToken = getAccessToken();
  let status: number;

  return fetch(url, {
    headers: {
      'content-type': 'application/json',
      Authorization: 'Bearer ' + accessToken,
    },
    mode: 'cors',
  })
    .then((response) => {
      status = response.status;

      return response.json();
    })
    .then((json) => {
      const isSuccess = status === 200;
      return {...json, status: status, isSuccess: isSuccess};
    })
    .catch((error) => {
      console.error(error);
      throw error;
    });
}

function combineBaseUrlAndRoute(route: string, baseUrl: string) {
  let result = baseUrl;
  if (baseUrl[baseUrl.length - 1] !== '/') result += '/';
  result += route;
  return result;
}

export function putAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'PUT', baseUrl);
}

export function postAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'POST', baseUrl);
}

export function deleteAsync(route: string, data: any, baseUrl: string) {
  return pAsync(route, data, 'DELETE', baseUrl);
}

function pAsync(route: string, data: any, method: string, baseUrl: string) {
  let url = combineBaseUrlAndRoute(route, baseUrl);
  let accessToken = getAccessToken();
  let status: number;
  let stringData = JSON.stringify(data);

  console.log(stringData);

  return fetch(url, {
    headers: {
      'content-type': 'application/json',
      Authorization: 'Bearer ' + accessToken,
    },
    method: method,
    body: stringData,
    mode: 'cors',
  })
    .then((response) => {
      status = response.status;

      //Status 204 will not have the content-length header
      let contentType = response.headers.get('Content-Type');
      if (contentType) {
        if (contentType.includes('application/json')) {
          console.log('Attempting response.json');
          return response.json();
        } else if (contentType.includes('text')) {
          return response.text();
        }
      } else return {};
    })
    .then((responseJson) => {
      if (typeof responseJson === 'string') {
        responseJson = {message: responseJson};
      }
      let isSuccess = status === 200 || status === 204;
      return {...responseJson, status: status, isSuccess: isSuccess};
    })
    .catch((error) => {
      console.log(error);
      throw error;
    });
}

export function getAccessToken() {
  return '';
}
