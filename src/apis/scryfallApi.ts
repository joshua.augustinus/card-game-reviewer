import {updateIsBusy} from '@src/reducers';
import {updateCards, updateSets} from '@src/reducers/scryfallBuilder';
import {HttpResponse} from '@src/types/api';
import {Alert} from 'react-native';
import * as baseApi from './baseApi';

const BASE_URL = 'https://api.scryfall.com';

export function getAsync(route: string) {
  return baseApi.getAsync(route, BASE_URL);
}

export function getCards(search_uri: string) {
  return function (dispatch: any) {
    dispatch(updateIsBusy(true));
    return baseApi.getAsync('', search_uri).then((result: HttpResponse) => {
      if (result.isSuccess) dispatch(updateCards(result.data));
      else {
        Alert.alert('Failed to get cards: ' + result.status);
      }
      dispatch(updateIsBusy(false));
    });
  };
}

export function getSets() {
  return function (dispatch: any) {
    dispatch(updateIsBusy(true));
    return getAsync('sets').then((result: HttpResponse) => {
      if (result.isSuccess) dispatch(updateSets(result.data));
      else {
        Alert.alert('Failed to get cards: ' + result.status);
      }
      dispatch(updateIsBusy(false));
    });
  };
}
