import {MD5} from 'react-native-crypto-js'; // npm i react-native-crypto-js
import Config from 'react-native-config';
import {ComicsResponse} from '@src/types/marvel';

const publicKey = Config.MARVEL_PUBLIC_KEY;
const privateKey = Config.MARVEL_PRIVATE_KEY;

const generateHash = (
  privateKey: string,
  publicKey: string,
  timestamp: number,
) => MD5(`${timestamp}${privateKey}${publicKey}`);

/**
 * Gets a list of characters
 * https://developer.marvel.com/docs#!/public/getCreatorCollection_get_0
 */
export const listCharacters = async (offset: number, countPerPage?: number) => {
  const timestamp = +new Date();
  const hash = generateHash(privateKey, publicKey, timestamp);
  let params = `offset=${offset}&apikey=${publicKey}&ts=${timestamp}&hash=${hash}`;
  if (countPerPage) {
    params += `&limit=${countPerPage}`;
  }

  return fetch(
    `https://gateway.marvel.com/v1/public/characters?${params}`,
  ).then((response) => response.json());
};

/**
 * Fetch characters but filtered by name
 * @param nameFilter Filter characters with names starting with this value
 * @param offset offset param in API
 * @param countPerPage equivalent to the limit param in the API
 */
export const listCharactersFiltered = async (
  nameFilter: string,
  offset: number,
  countPerPage: number,
) => {
  const timestamp = +new Date();
  const hash = generateHash(privateKey, publicKey, timestamp);
  let params = `offset=${offset}&apikey=${publicKey}&ts=${timestamp}&hash=${hash}`;

  if (countPerPage) {
    params += `&limit=${countPerPage}`;
  }
  if (nameFilter?.length > 0) {
    params += `&nameStartsWith=${nameFilter}`;
  }

  const url = `https://gateway.marvel.com/v1/public/characters?${params}`;

  return fetch(url)
    .then((response) => response.json())
    .then((json) => {
      json.filter = nameFilter;
      return json;
    });
};

/**
 * Fetch comics for a character
 */
export const fetchComics = async (
  characterId: number,
  offset: number,
  countPerPage: number,
) => {
  console.log('Fetching with offset ' + offset);
  const timestamp = +new Date();
  const hash = generateHash(privateKey, publicKey, timestamp);
  let params = `offset=${offset}&apikey=${publicKey}&ts=${timestamp}&hash=${hash}`;

  if (countPerPage) {
    params += `&limit=${countPerPage}`;
  }

  const url = `https://gateway.marvel.com/v1/public/characters/${characterId}/comics?${params}`;
  return fetch(url)
    .then((response) => response.json())
    .then((responseJson: ComicsResponse) => {
      for (let i = 0; i < responseJson.data.results.length; i++) {
        responseJson.data.results[i].customId =
          offset.toString() + '_' + i.toString();
      }

      return responseJson;
    });
};
