const CColors = {
  background: '#f5f8fb',
  primary: '#000000',
  button: {
    primary: '#E54024',
    light: 'white',
    secondary: '#5b6ff3',
  },
  buttonText: {
    primary: 'white',
    light: 'black',
    secondary: 'white',
  },
  danger: '#E54024',
  text: {
    default: '#444',
    secondary: '#E54024',
    light: 'white',
    primary: '#000000',
    accent: '#2B61A4',
  },
};

export {CColors};
