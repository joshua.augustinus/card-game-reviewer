import {Account} from '@screens/BottomNavigator/Account';
import {BottomNavigator} from '@screens/BottomNavigator/';
import {Random} from '@screens/BottomNavigator/Random';
import {Stocks} from '@screens/BottomNavigator/Stocks';
import * as React from 'react';
import {BottomNavigation} from 'react-native-paper';

/**
 * Uses the BottomNavigation component from react-native-paper
 * https://callstack.github.io/react-native-paper/bottom-navigation.html
 */
const PaperBottomNav = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {
      key: 'account',
      title: 'Account',
      icon: 'account',
      accessibilityLabel: 'account tab',
    },
    {
      key: 'random',
      title: 'Random',
      icon: 'dice-multiple',
      accessibilityLabel: 'random tab',
    },
    {
      key: 'stocks',
      title: 'Stocks',
      icon: 'chart-line',
      accessibilityLabel: 'stocks tab',
    },
    {key: 'home', title: 'Home', icon: 'home', accessibilityLabel: 'home tab'},
  ]);

  const renderScene = BottomNavigation.SceneMap({
    account: () => <Account />,
    random: () => <Random />,
    stocks: () => <Stocks />,
    home: () => <BottomNavigator />,
  });

  return (
    <BottomNavigation
      navigationState={{index, routes}}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};

export {PaperBottomNav};
