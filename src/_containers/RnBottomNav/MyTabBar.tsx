import {View, Pressable} from 'react-native';
import * as React from 'react';
import {StyleSheet} from 'react-native';
import {CColors} from '@src/colors';
import {CText} from '@components/CText';

/**
 * Copeid from https://reactnavigation.org/docs/bottom-tab-navigator/
 */

interface Props {
  state: any;
  descriptors: any;
  navigation: any;
}
const ICON_SIZE = 26;

const MyTabBar = (props: Props) => {
  const state = props.state;
  const descriptors = props.descriptors;
  const navigation = props.navigation;

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route: any, index: number) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        return (
          <Pressable
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={styles.iconWrapper}>
            {options.tabBarIcon({color: CColors.text.light, size: ICON_SIZE})}
            {isFocused && (
              <CText type="small" color="light">
                {label}
              </CText>
            )}
          </Pressable>
        );
      })}
    </View>
  );
};
export default React.memo(MyTabBar);

const styles = StyleSheet.create({
  container: {
    height: 60,
    paddingHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: 'black',
  },
  iconWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
});
