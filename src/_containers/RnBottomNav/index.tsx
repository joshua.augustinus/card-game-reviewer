import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Account} from '@screens/BottomNavigator/Account';
import {BottomNavigator} from '@screens/BottomNavigator/';
import {Random} from '@screens/BottomNavigator/Random';
import {Stocks} from '@screens/BottomNavigator/Stocks';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MyTabBar from './MyTabBar';
import {SafeAreaView} from 'react-native';

const Tab = createBottomTabNavigator();

function RnBottomNav() {
  return (
    <SafeAreaView style={{flex: 1}}>
      <Tab.Navigator
        tabBar={(props) => <MyTabBar {...props} />}
        tabBarOptions={{
          activeTintColor: '#e91e63',
        }}>
        <Tab.Screen
          name="Account"
          component={Account}
          options={{
            tabBarLabel: 'Account',
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons
                name="account"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Random"
          component={Random}
          options={{
            tabBarLabel: 'Random',
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons
                name="dice-multiple"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Stocks"
          component={Stocks}
          options={{
            tabBarLabel: 'Stocks',
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons
                name="chart-line"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="BottomNavigator"
          component={BottomNavigator}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
          }}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
}

export {RnBottomNav};
