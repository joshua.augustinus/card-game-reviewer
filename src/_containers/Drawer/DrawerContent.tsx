import React from 'react';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Divider, Drawer} from 'react-native-paper';
import {navigate} from '@src/RootNavigation';
import {DrawerItem} from '@src/types';
import {drawerItems} from './drawerItems';
import {View} from 'react-native';

const DrawerContent = (props: any) => {
  const [active, setActive] = React.useState('Introduction');

  const onItemPressed = (id: string) => {
    setActive(id);
    navigate(id);
  };

  const getDrawerItems = (array: DrawerItem[]) => {
    return array.map((item) => {
      return (
        <View key={item.screenName}>
          {item.hasDivider && <Divider />}
          <Drawer.Item
            label={item.label}
            onPress={() => onItemPressed(item.screenName)}
            active={active === item.screenName}></Drawer.Item>
        </View>
      );
    });
  };

  return (
    <DrawerContentScrollView {...props}>
      {getDrawerItems(drawerItems)}
    </DrawerContentScrollView>
  );
};

export {DrawerContent};
