import React, {useState} from 'react';
import AppHeader from '@components/AppHeader';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {SafeAreaView, StyleSheet} from 'react-native';
import {navigationRef} from '@src/RootNavigation';
import {useDispatch} from 'react-redux';
import {updateAppHeaderTitle} from '@src/reducers';
import {normaliseText} from '@src/util/textUtil';
import * as scryfallApi from '@apis/scryfallApi';
import {DrawerContent} from './DrawerContent';
import {allScreens} from './drawerItems';

const Drawer = createDrawerNavigator();

const DrawerContainer = (props: any) => {
  const dispatch = useDispatch();
  const [isReady, setIsReady] = useState(false);

  /**
   * Call scryfall once to get Sets
   */
  React.useEffect(() => {
    dispatch(scryfallApi.getSets());
  }, []);

  React.useEffect(() => {
    const unsubscribe = navigationRef.current?.addListener(
      'state',
      (e: any) => {
        const index = e.data?.state?.index;
        if (index >= 0) {
          const currentRoute = e.data.state.routes[index];
          const title = currentRoute.name;
          dispatch(updateAppHeaderTitle(title));
        } else {
          console.log('Navigation event', e);
        }
      },
    );

    return unsubscribe;
  }, []);

  React.useEffect(() => {
    //this is a hack to prevent drawer navigator from blinking on start
    //https://github.com/react-navigation/react-navigation/issues/7561
    setTimeout(() => setIsReady(true), 1);
  }, []);

  const drawerStyle = isReady
    ? styles.normalDrawerStyle
    : styles.initialDrawerStyle;

  return (
    <SafeAreaView style={{flex: 1}}>
      <AppHeader showMenuToggle={true} />

      <Drawer.Navigator
        openByDefault={false}
        drawerType="slide"
        initialRouteName="Introduction"
        drawerStyle={drawerStyle}
        drawerContent={(props) => <DrawerContent {...props} />}>
        {allScreens.map((item) => (
          <Drawer.Screen
            key={item.screenName}
            name={item.screenName}
            component={item.component}
          />
        ))}
      </Drawer.Navigator>
    </SafeAreaView>
  );
};

export default DrawerContainer;

const styles = StyleSheet.create({
  initialDrawerStyle: {
    width: 0,
    backgroundColor: 'white',
  },
  normalDrawerStyle: {
    backgroundColor: 'white',
  },
});
