import {ScryfallSets} from '@screens/ScryfallSets';
import {Introduction} from '@screens/Introduction';
import {Login} from '@screens/Login';
import {MobilePhone} from '@screens/MobilePhone';
import {BottomNavigator} from '@screens/BottomNavigator';
import {ScryfallCards} from '@screens/ScryfallCards';
import {DrawerItem} from '@src/types';
import {Buttons} from '@screens/Buttons';
import {ScryfallDocumentation} from '@screens/ScryfallDocumentation';
import {Typography} from '@screens/Typography';
import {MarvelHome} from '@screens/Marvel/MarvelHome';
import {EnvFiles} from '@screens/Marvel/EnvFiles';
import {FetchingCharacters} from '@screens/Marvel/FetchingCharacters';
import {EndlessScroll} from '@screens/Marvel/EndlessScroll';
import {EndlessScrollExample} from '@screens/Marvel/EndlessScrollExample';
import {FilterResults} from '@screens/Marvel/FilterResults';
import {FilterResultsExample} from '@screens/Marvel/FilterResultsExample';
import {CharacterDetails} from '@screens/Marvel/CharacterDetails';
import {CharacterInfo} from '@screens/Marvel/CharacterInfo';
import {SelectableListItem} from '@screens/Marvel/SelectableListItem';
import {SelectableListItemExample} from '@screens/Marvel/SelectableListItemExample';
import {FetchingComics} from '@screens/Marvel/FetchingComics';
import {Comics} from '@screens/Marvel/Comics';
import {ChartPopup} from '@screens/Marvel/ChartPopup';
import {DrawerNavigatorProblems} from '@screens/DrawerNavigatorProblems';
import {LoginExample} from '@screens/Login/LoginExample';
import {ForgetfulViewExample} from '@screens/DrawerNavigatorProblems/ForgetfulViewExample';
import {ClearableTextInput} from '@screens/ClearableTextInput';

export const drawerItems: DrawerItem[] = [
  {label: 'Introduction', screenName: 'Introduction', component: Introduction},

  {
    label: 'Buttons',
    hasDivider: true,
    screenName: 'Buttons',
    component: Buttons,
  },
  {label: 'Typography', screenName: 'Typography', component: Typography},
  {label: 'Login Form', screenName: 'Login', component: Login},
  {
    label: 'DrawerNavigator Problems',
    screenName: 'DrawerNavigator Problems',
    component: DrawerNavigatorProblems,
  },
  {
    label: 'Clearable Text Input',
    screenName: 'Clearable Text Input',
    component: ClearableTextInput,
  },
  {
    label: 'Mobile Phone Input',
    screenName: 'MobilePhone',
    component: MobilePhone,
  },
  {
    label: 'BottomNavigator',
    screenName: 'BottomNavigator',
    component: BottomNavigator,
  },
  {
    label: 'Scryfall',
    screenName: 'Scryfall Documentation',
    hasDivider: true,
    component: ScryfallDocumentation,
  },
  {
    label: '- Sets',
    screenName: 'Scryfall Sets',

    component: ScryfallSets,
  },
  {label: '- Cards', screenName: 'Scryfall Cards', component: ScryfallCards},
  {
    label: 'Marvel',
    screenName: 'Marvel',
    hasDivider: true,
    component: MarvelHome,
  },
  {
    label: '- Hiding Secrets',
    screenName: 'Hiding Secrets',
    component: EnvFiles,
  },
  {
    label: '- Fetching Characters',
    screenName: 'Fetching Characters',
    component: FetchingCharacters,
  },
  {
    label: '- Endless Scroll',
    screenName: 'Endless Scroll',
    component: EndlessScroll,
  },
  {
    label: '- Filtering Results',
    screenName: 'Filtering Results',
    component: FilterResults,
  },
  {
    label: '- Fetching Comics',
    screenName: 'Fetching Comics',
    component: FetchingComics,
  },
  {
    label: '- Chart Popup',
    screenName: 'Chart Popup',
    component: ChartPopup,
  },
  {
    label: '- Character Details',
    screenName: 'Character Details',
    component: CharacterDetails,
  },
  {
    label: '- Selectable List Item',
    screenName: 'Selectable List Item',
    component: SelectableListItem,
  },
];

/**
 * These are screens that are accessible via navigation but not accessible by Drawer
 */
export const allScreens = [
  ...drawerItems,
  {screenName: 'Endless Scroll Example', component: EndlessScrollExample},
  {screenName: 'Filtering Results Example', component: FilterResultsExample},
  {screenName: 'Character Info', component: CharacterInfo},
  {
    screenName: 'Selectable List Item Example',
    component: SelectableListItemExample,
  },
  {
    screenName: 'Comics',
    component: Comics,
  },
  {
    screenName: 'Login Example',
    component: LoginExample,
  },
  {
    screenName: 'Forgetful View Example',
    component: ForgetfulViewExample,
  },
];
