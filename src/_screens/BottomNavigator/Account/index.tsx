import React, {useState} from 'react';
import {CCard} from '@src/_components/CCard';

import {View, ScrollView} from 'react-native';
import styles from './styles';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Title} from 'react-native-paper';

import {AccountButton} from './AccountButton';
import {CButton} from '@src/_components/CButton';
import {CText} from '@components/CText';
import {SlidingView} from '@components/SlidingView';

const Account = () => {
  const [translateX, setTranslateX] = useState(0);
  //Hooks can't be called inside loops?

  //icons:
  //https://oblador.github.io/react-native-vector-icons/
  const list = [
    {
      title: 'Cards',
      icon: 'credit-card',
    },
    {
      title: 'Settings',
      icon: 'cog',
    },
    {
      title: 'Terms',
      icon: 'file-document-edit-outline',
    },
    {
      title: 'Privacy Policy',
      icon: 'file-document-outline',
    },
  ];

  const startAnimation = () => {
    setTranslateX(800);
    setTimeout(function () {
      setTranslateX(0);
    }, 3500);
  };

  return (
    <ScrollView>
      <CCard>
        <Title>John Doe</Title>
        <CText type="normal">john.doe@gmail.com</CText>
        <CText type="normal">0498754321</CText>
        <CText type="normal">Balance Available: $0</CText>
      </CCard>
      <CCard>
        <SlidingView toValue={translateX} delay={0}>
          <CButton
            small
            onPress={startAnimation}
            title="Click a button to show animations"
            type="light"
          />
        </SlidingView>
        <SlidingView style={styles.buttonRow} toValue={translateX} delay={200}>
          <AccountButton
            style={styles.buttonStyle}
            title="How To"
            onPress={startAnimation}
          />
          <View style={{width: 5}} />
          <AccountButton
            style={styles.buttonStyle}
            title="Help Center"
            onPress={startAnimation}
          />
        </SlidingView>
        <SlidingView style={styles.buttonRow} toValue={translateX} delay={400}>
          <AccountButton
            style={styles.buttonStyle}
            title="Get In Touch"
            onPress={startAnimation}
          />
          <View style={{width: 5}} />
          <AccountButton
            style={styles.buttonStyle}
            title="Logout"
            onPress={startAnimation}
          />
        </SlidingView>
        <View style={{marginTop: 10}}>
          {list.map((item, i) => (
            <SlidingView key={i} toValue={translateX} delay={600 + i * 200}>
              <ListItem bottomDivider onPress={startAnimation}>
                <Icon name={item.icon} size={20} style={{opacity: 0.5}} />
                <ListItem.Content>
                  <ListItem.Title style={{paddingVertical: 5}}>
                    {item.title}
                  </ListItem.Title>
                </ListItem.Content>
                <ListItem.Chevron />
              </ListItem>
            </SlidingView>
          ))}
        </View>
        <SlidingView
          startValue={-800}
          toValue={translateX - 800}
          style={styles.disclaimerContainer}>
          <CCard danger style={{width: '100%'}}>
            <CText type="normal">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </CText>
          </CCard>
        </SlidingView>
      </CCard>
      <View style={{height: 20}}></View>
    </ScrollView>
  );
};

export {Account};
