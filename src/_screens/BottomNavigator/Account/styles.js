import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  buttonRow: {flexDirection: 'row', flexWrap: 'wrap', marginTop: 5},
  buttonStyle: {
    flex: 1,
  },
  disclaimerContainer: {position: 'absolute', width: '100%', left: -5, top: -5},
});

export default styles;
