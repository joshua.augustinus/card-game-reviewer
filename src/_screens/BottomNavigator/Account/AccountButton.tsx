import React from 'react';

import {CButton} from '@src/_components/CButton';
import {View} from 'react-native';

interface Props {
  style: any;
  title: string;
  onPress: () => void;
}

const AccountButton = (props: Props) => {
  return (
    <View style={{flex: 1}}>
      <CButton
        small
        type="primary"
        style={props.style}
        title={props.title}
        onPress={props.onPress}
      />
    </View>
  );
};

export {AccountButton};
