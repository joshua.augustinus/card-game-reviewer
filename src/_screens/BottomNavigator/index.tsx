import React, {useDebugValue} from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import {CHighlight} from '@components/CHighlight';
import {CButton} from '@components/CButton';
import {updateContainerType} from '@src/reducers';
import {useDispatch} from 'react-redux';
import {StyleSheet} from 'react-native';

const BottomNavigator = () => {
  const dispatch = useDispatch();
  const usePaperBottomNav = () => {
    dispatch(updateContainerType('PaperBottomNav'));
  };

  const useDrawerNav = () => {
    dispatch(updateContainerType('Drawer'));
  };
  const useRnBottomNav = () => {
    dispatch(updateContainerType('RnBottomNav'));
  };
  return (
    <DeferredRender>
      <CCard>
        <CTitle>Custom Bottom Navigator</CTitle>
        <CText type="paragraph">
          For performance reasons you should avoid nesting navigators. It's
          better to pick just one and go with it. If you look at{' '}
          <CHighlight>App.tsx</CHighlight> you can see that I have functionality
          to switch between nevigation types (e.g. Drawer and Bottom) rather
          than use multiple at the same time.
        </CText>
      </CCard>

      <CCard>
        <CTitle>Third Party Navigators</CTitle>
        <CText type="normal" color="accent">
          react-native-paper BottomNavigation
        </CText>
        <CText type="paragraph">
          This navigator was laggy. You can test it by pressing the button
          below. I had similar issues with{' '}
          <CHighlight>@react-navigation/material-bottom-tabs</CHighlight>.
        </CText>
        <CButton
          style={styles.cmSpace}
          small
          onPress={usePaperBottomNav}
          title="react-native-paper Bottom Navigator"></CButton>
        <CText type="normal" color="accent">
          react-navigation createBottomTabNavigator
        </CText>
        <CText type="paragraph">
          This navigator had much better performance but took a bit of work to
          setup.
        </CText>
        <CButton
          small
          onPress={useRnBottomNav}
          title="react-navigation Bottom Navigator"></CButton>
      </CCard>
      <CCard>
        <CTitle>Back to Drawer Navigator</CTitle>
        <CText type="paragraph">
          Once you have finished testing the bottom navigators you can return to
          the drawer navigation by pressing the button below.
        </CText>
        <CButton
          small
          onPress={useDrawerNav}
          title="Drawer Navigator"></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {BottomNavigator};

const styles = StyleSheet.create({
  cmSpace: {
    marginBottom: 40,
  },
});
