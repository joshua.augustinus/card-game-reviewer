import {CWebView} from '@components/CWebView';
import React from 'react';

const Stocks = () => {
  return <CWebView url="https://www.mtgstocks.com/news" />;
};

export {Stocks};
