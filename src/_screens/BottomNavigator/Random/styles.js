import {elevations} from '@src/util/elevations';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  iconButtonContainer: {
    position: 'absolute',
    right: 20,
    bottom: 10,
    elevation: elevations.MEDIUM,
  },
});

export default styles;
