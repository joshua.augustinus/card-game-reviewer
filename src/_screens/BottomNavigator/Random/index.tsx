import {CActivityIndicator} from '@components/CActivityIndicator';
import {CIconButton} from '@components/CIconButton';
import {CWebView} from '@components/CWebView';
import React, {useState} from 'react';
import {View} from 'react-native';
import styles from './styles';

const DEFAULT_URL = 'https://scryfall.com/random';
//<CWebView url="https://scryfall.com/random" />
const Random = () => {
  const [url, setUrl] = useState(DEFAULT_URL);

  const reload = () => {
    setUrl(DEFAULT_URL + '?t=' + Date.now());
  };
  return (
    <View style={{flex: 1}}>
      <CWebView url={url} />
      <View style={styles.iconButtonContainer}>
        <CIconButton iconName="reload" onPress={reload} />
      </View>
    </View>
  );
};

export {Random};
