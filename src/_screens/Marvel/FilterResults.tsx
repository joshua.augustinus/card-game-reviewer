import React, {useEffect} from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import {useNavigation} from '@react-navigation/native';
import {CButton} from '@components/CButton';
import styles from './styles';

const FilterResults = () => {
  const navigation = useNavigation();
  const seeExample = () => {
    navigation.navigate('Filtering Results Example');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Filtering Results</CTitle>
        <CText type="paragraph">
          Now we will add a TextInput to filter our results. Every time the
          filter changes we call the API but pass in a "nameStartsWith" param.
        </CText>

        <CButton
          style={styles.exampleButton}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {FilterResults};
