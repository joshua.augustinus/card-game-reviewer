import React, {useEffect, useRef} from 'react';
import {CText} from '@components/CText';
import {fetchComics} from '@apis/marvelApi';
import {useState} from 'react';
import {Comic, ComicsResponse} from '@src/types/marvel';
import {CActivityIndicator} from '@components/CActivityIndicator';
import {FlatList} from 'react-native-gesture-handler';
import {View} from 'react-native';
import {ActivityIndicatorType} from '@src/types';
import styles from './styles';

const renderItem = ({item, index}: {item: Comic; index: number}) => {
  return <CText type="paragraph">{item.title}</CText>;
};

const keyExtractor = (item: Comic) => {
  return item.customId;
};

/**
 * https://developer.marvel.com/docs#!/public/getComicsCharacterCollection_get_2
 * Will get 409 error if Limit greater than 100
 */
const ITEMS_PER_PAGE = 100;

interface Props {
  characterId: number;
}

const initialState = {
  offset: 0,
  totalResults: -1,
};

/**
 * For some reason this component doesn't unmount if you leave the screen
 * by pressing the back button
 */
const ComicsComponent = (props: Props) => {
  const [comics, setComics] = useState<Comic[]>(undefined);
  const [isBusy, setIsBusy] = useState(true);
  const [activityIndicatortype, setActivityIndicatorType] = useState<
    ActivityIndicatorType
  >('absolute');

  const state = useRef(initialState);
  const characterId = props.characterId;

  /**
   * This effect gets called on load and should always have offset 0
   */
  useEffect(() => {
    state.current = initialState;
    setActivityIndicatorType('absolute');
    setIsBusy(true);
    setComics(undefined);
    const offset = 0;

    fetchComics(characterId, offset, ITEMS_PER_PAGE).then(
      (response: ComicsResponse) => {
        setIsBusy(false);
        setComics(response.data.results);
        setActivityIndicatorType('small');
        state.current.totalResults = response.data.total;
      },
    );

    return () => {
      console.log('Component unmounted');
    };
  }, [props.characterId]);

  const endReachedHandler = () => {
    if (
      state.current.totalResults > 0 &&
      state.current.offset < state.current.totalResults
    ) {
      if (isBusy) {
        return;
      }
      const newOffset = state.current.offset + ITEMS_PER_PAGE;
      state.current.offset = newOffset;
      setIsBusy(true);
      fetchComics(characterId, newOffset, ITEMS_PER_PAGE).then(
        (response: ComicsResponse) => {
          //In order to get latest value of characters we store it in ref
          setIsBusy(false);
          if (newOffset > response.data.total) {
            return;
          }
          //do nothing since we reached the end
          else {
            console.log(`Offset: ${newOffset} Comics length:${comics.length}`);

            const newComics = [...comics, ...response.data.results];
            setComics(newComics);
          }
        },
      );
    }
  };

  const content = () => {
    if (comics) {
      return (
        <FlatList
          data={comics}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          onEndReachedThreshold={2}
          onEndReached={endReachedHandler}
        />
      );
    } else {
      return null;
    }
  };

  return (
    <View style={styles.flex}>
      {content()}

      {isBusy && <CActivityIndicator type={activityIndicatortype} />}
    </View>
  );
};

export {ComicsComponent};
