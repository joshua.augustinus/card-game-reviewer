import React from 'react';
import {CText} from '@components/CText';
import {DeferredRender} from '@components/DeferredRender';
import {CCard} from '@components/CCard';
import {CTitle} from '@components/CTitle';
import {CButton} from '@components/CButton';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const SelectableListItem = () => {
  const navigation = useNavigation();

  const seeExample = () => {
    navigation.navigate('Selectable List Item Example');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Selectable List Item</CTitle>
        <CText type="paragraph">
          Now we need to replace our text search results with pressable buttons.
          On press, we should navigate to the Character Info page.
        </CText>

        <CButton
          style={styles.exampleButton}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {SelectableListItem};
