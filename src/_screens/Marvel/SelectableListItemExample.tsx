import React, {useRef} from 'react';
import {useState} from 'react';
import {Character} from '@src/types/marvel';
import {PressableText} from '@components/PressableText';
import {useDispatch} from 'react-redux';
import {updateCharacterSelected} from '@src/reducers/marvelBuilder';
import {useNavigation} from '@react-navigation/native';
import {MarvelSearch} from './MarvelSearch';

interface UseEffectDependencies {
  offset: number;
  characters: Character[];
  filter: string;
}

const intialState: UseEffectDependencies = {
  offset: 0,
  characters: [],
  filter: '',
};

const initialNonRenderState = {
  totalResults: -1,
  pendingRequests: 0,
};

const SelectableListItemExample = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  /**
   * Handler for when item pressed
   */
  const itemPressed = React.useCallback((data: Character) => {
    dispatch(updateCharacterSelected(data));
    navigation.navigate('Character Info');
  }, []);

  /**
   * Function to render items
   */
  const renderItem = ({item, index}: {item: Character; index: number}) => {
    return (
      <PressableText data={item} onPress={itemPressed} textType="paragraph">
        {item.name}
      </PressableText>
    );
  };

  return <MarvelSearch renderItemFunc={renderItem} />;
};

export {SelectableListItemExample};
