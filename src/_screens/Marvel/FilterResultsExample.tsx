import React from 'react';
import {CText} from '@components/CText';

import {Character} from '@src/types/marvel';

import {MarvelSearch} from './MarvelSearch';
const renderItem = ({item, index}: {item: Character; index: number}) => {
  return <CText type="paragraph">{item.name}</CText>;
};

/**
 * This component actually has some bugs because when the user types something in
 * then multiple requests can trigger due to the filter changing and also due
 * to end of list reached. The has been fixed in SelectableListItemExample
 */
const FilterResultsExample = () => {
  return <MarvelSearch renderItemFunc={renderItem} />;
};

export {FilterResultsExample};
