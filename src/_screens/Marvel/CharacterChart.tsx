import {Character} from '@src/types/marvel';
import {useScreenInfo} from '@src/util/customHooks';
import React from 'react';
import {View} from 'react-native';

import {VictoryBar, VictoryChart, VictoryTheme} from 'victory-native';

interface Props {
  character: Character;
}

const widthRatio = 0.95; //need to test with Captain America who has huge numbers

const convertCharacterToData = (character: Character) => {
  const data = [
    {x: 'Comics', y: character.comics.available},
    {x: 'Series', y: character.series.available},
    {x: 'Stories', y: character.stories.available},
    {x: 'Events', y: character.events.available},
  ];

  console.log(data);

  return data;
};

export const CharacterChart = (props: Props) => {
  const screenInfo = useScreenInfo();
  const character = props.character;
  if (
    character.comics.available +
      character.series.available +
      character.stories.available +
      character.events.available ===
    0
  )
    return null;

  return (
    <View style={{alignItems: 'flex-end', paddingLeft: 30}}>
      <VictoryChart
        height={250}
        theme={VictoryTheme.material}
        width={screenInfo.width * widthRatio}
        domainPadding={{x: 60}}>
        <VictoryBar
          alignment="middle"
          barRatio={1}
          data={convertCharacterToData(props.character)}
        />
      </VictoryChart>
    </View>
  );
};
