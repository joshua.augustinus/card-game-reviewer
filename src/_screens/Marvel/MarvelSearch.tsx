import React, {useEffect, useRef} from 'react';
import {listCharactersFiltered} from '@apis/marvelApi';
import {useState} from 'react';
import {Character, CharactersResponse} from '@src/types/marvel';
import {CActivityIndicator} from '@components/CActivityIndicator';
import {FlatList} from 'react-native-gesture-handler';
import {ListRenderItem, StyleSheet, View} from 'react-native';
import {ActivityIndicatorType} from '@src/types';
import {CCard} from '@components/CCard';
import {CTextInput} from '@components/CTextInput';

const keyExtractor = (item: Character) => {
  return item.id.toString();
};

const ITEMS_PER_PAGE = 50;

const initialState = {
  totalResults: -1,
  offset: 0,
  filter: '',
};

interface Props {
  renderItemFunc: ListRenderItem<Character>;
}

const MarvelSearch = (props: Props) => {
  const [characters, setCharacters] = useState<Character[]>([]);
  const [isBusy, setIsBusy] = useState(true);
  const [activityIndicatortype, setActivityIndicatorType] = useState<
    ActivityIndicatorType
  >('absolute');

  const [filter, setFilter] = useState('');

  const stateRef = useRef(initialState);

  const fetchData = (offset: number, filterInput: string) => {
    setIsBusy(true);

    listCharactersFiltered(filterInput, offset, ITEMS_PER_PAGE).then(
      (response: CharactersResponse) => {
        //Inside this handler references to variables may be out of date
        //Use useRef to get latest stuff

        //Ignore responses that are for a different filter
        if (response.filter !== stateRef.current.filter) {
          console.log('Wrong response');
          return;
        }

        setIsBusy(false);

        if (offset === 0) {
          stateRef.current.totalResults = response.data.total;
          setCharacters(response.data.results);
          setActivityIndicatorType('small');
        } else if (offset > response.data.total) {
          return;
        }
        //do nothing since we reached the end
        else {
          const updatedCharacters = [...characters, ...response.data.results];

          setCharacters(updatedCharacters);
        }
      },
    );
  };

  useEffect(() => {
    console.log('Initial fetch');
    fetchData(0, '');
  }, []);

  const endReachedHandler = () => {
    console.log('End reached');
    const state = stateRef.current;
    if (state.totalResults > 0 && state.totalResults > state.offset) {
      const newOffset = state.offset + ITEMS_PER_PAGE;
      state.offset = newOffset;
      fetchData(newOffset, filter);
    } else {
      console.log('End reached but ignoring');
    }
  };

  const content = () => {
    if (characters) {
      return (
        <FlatList
          data={characters}
          renderItem={props.renderItemFunc}
          keyExtractor={keyExtractor}
          onEndReachedThreshold={2}
          onEndReached={endReachedHandler}
        />
      );
    } else {
      return null;
    }
  };

  const filterChanged = (text: string) => {
    setFilter(text);
    stateRef.current.offset = 0;
    stateRef.current.filter = text;
    //When user types text reset offset to zero
    fetchData(stateRef.current.offset, stateRef.current.filter);
  };

  return (
    <View style={styles.flex}>
      <CCard>
        <CTextInput
          clearable
          accessibilityLabel="Search Filter"
          value={filter}
          label="Filter Results"
          onChangeText={filterChanged}
        />
      </CCard>
      <View style={styles.contentContainer}>{content()}</View>

      {isBusy && <CActivityIndicator type={activityIndicatortype} />}
    </View>
  );
};

export {MarvelSearch};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20,
  },
});
