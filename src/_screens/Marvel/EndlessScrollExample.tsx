import React, {useEffect, useRef} from 'react';
import {CText} from '@components/CText';
import {listCharacters} from '@apis/marvelApi';
import {useState} from 'react';
import {Character, CharactersResponse} from '@src/types/marvel';
import {CActivityIndicator} from '@components/CActivityIndicator';
import {FlatList} from 'react-native-gesture-handler';
import {StyleSheet, View} from 'react-native';
import {ActivityIndicatorType} from '@src/types';

const renderItem = ({item, index}: {item: Character; index: number}) => {
  return <CText type="paragraph">{item.name}</CText>;
};

const keyExtractor = (item: Character) => {
  return item.id.toString();
};

const ITEMS_PER_PAGE = 20;

const EndlessScrollExample = () => {
  const [characters, setCharacters] = useState<Character[]>(undefined);
  const charactersRef = useRef(null);
  const [offset, setOffset] = useState(0);
  const [isBusy, setIsBusy] = useState(true);
  const [activityIndicatortype, setActivityIndicatorType] = useState<
    ActivityIndicatorType
  >('absolute');

  /**
   * This useEffect has a dependency on characters but we don't want to call the effect
   * every time characters changes. So to get the latest version of characters we just
   * use a reference to it.
   */
  useEffect(() => {
    setIsBusy(true);

    console.log('Fetching characters with offset: ' + offset);
    listCharacters(offset, ITEMS_PER_PAGE).then(
      (response: CharactersResponse) => {
        //In order to get latest value of characters we store it in ref
        setIsBusy(false);
        if (offset === 0) {
          console.log('Render List first time');
          charactersRef.current = response.data.results;
          setCharacters(charactersRef.current);
          setActivityIndicatorType('small');
        } else if (offset > response.data.total) {
          console.log('Do nothing');
          return;
        }
        //do nothing since we reached the end
        else {
          console.log('Adding to list');
          charactersRef.current = [
            ...charactersRef.current,
            ...response.data.results,
          ];
          setCharacters(charactersRef.current);
        }
      },
    );
  }, [offset]);

  const endReachedHandler = () => {
    setOffset(offset + ITEMS_PER_PAGE);
  };

  const content = () => {
    if (characters) {
      console.log('Render flatlist');
      return (
        <FlatList
          data={characters}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          onEndReachedThreshold={2}
          onEndReached={endReachedHandler}
        />
      );
    } else {
      return null;
    }
  };

  return (
    <View style={styles.container}>
      {content()}

      {isBusy && <CActivityIndicator type={activityIndicatortype} />}
    </View>
  );
};

export {EndlessScrollExample};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
});
