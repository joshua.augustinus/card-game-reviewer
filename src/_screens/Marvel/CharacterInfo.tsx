import React from 'react';
import {CText} from '@components/CText';
import {useSelector} from 'react-redux';
import {RootState} from '@src/types';
import {StyleSheet, View} from 'react-native';
import {ComicsComponent} from './ComicsComponent';
import {ChartPopupComponent} from './ChartPopupComponent';
import {ForgetfulView} from '@components/ForgetfulView';

const CharacterInfo = () => {
  const character = useSelector(
    (state: RootState) => state.marvelState.characterSelected,
  );
  return (
    <ForgetfulView style={styles.container}>
      <CText type="title" color="accent">
        Character Profile
      </CText>
      <CText type="subHeading" color="accent">
        Name
      </CText>
      <CText type="paragraph">{character.name}</CText>
      <CText type="subHeading" color="accent">
        Description
      </CText>
      <CText type="paragraph">{character.description}</CText>

      <CText type="title" color="accent">
        Comics
      </CText>
      <ComicsComponent characterId={character.id} />
      <ChartPopupComponent character={character} />
    </ForgetfulView>
  );
};

export {CharacterInfo};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
    flex: 1,
  },
});
