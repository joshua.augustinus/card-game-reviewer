import React, {useState} from 'react';
import styles from './styles';
import {PressableText} from '@components/PressableText';
import {Modal} from 'react-native-paper';
import {Text, View} from 'react-native';
import {Character} from '@src/types/marvel';
import {CharacterChart} from './CharacterChart';
import {CText} from '@components/CText';

interface Props {
  character: Character;
}

/**
 * The modal will only show up correctly if it's the last component
 * Otherwise it will have the wrong z-index
 */
const ChartPopupComponent = (props: Props) => {
  const [isModalVisisble, setIsModalVisible] = useState(false);
  const onPress = () => {
    setIsModalVisible(true);
  };

  return (
    <>
      <PressableText data={null} textType="normal" onPress={onPress}>
        View Statistics
      </PressableText>
      <Modal
        contentContainerStyle={styles.modalContainer}
        visible={isModalVisisble}
        onDismiss={() => setIsModalVisible(false)}>
        <View style={styles.modalView}>
          <CText type="title" color="accent">
            Statistics
          </CText>
          <CharacterChart character={props.character} />
        </View>
      </Modal>
    </>
  );
};

export {ChartPopupComponent};
