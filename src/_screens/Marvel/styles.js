import { elevations } from '@src/util/elevations';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    exampleButton: {
      marginTop: 20,
    },
    flex: {
      flex: 1,
    },
    contentContainer: {
      flex: 1,
      marginHorizontal: 20,
      marginTop: 20,
    },
    modalView: {backgroundColor: 'white', paddingTop: 10, borderRadius: 10, alignItems:'center'},
    modalContainer: {width: '95%', alignSelf: 'center', elevation:elevations.HIGH},
  });

  export default styles;