import {Character} from '@src/types/marvel';

export const testCharacter: Character = {
  id: 1009223,
  name: 'Fake name',
  description: 'This character has lots of comics',
  comics: {
    available: 2,
  },
  series: {
    available: 1,
  },
  stories: {
    available: 2,
  },
  events: {
    available: 5,
  },
};
