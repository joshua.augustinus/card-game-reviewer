import React from 'react';
import {View} from 'react-native';
import {RootState} from '@src/types';
import {useSelector} from 'react-redux';
import styles from './styles';
import {ComicsComponent} from './ComicsComponent';

const Comics = () => {
  const characterId = useSelector(
    (state: RootState) => state.marvelState.characterSelected.id,
  );

  return (
    <View style={styles.contentContainer}>
      <ComicsComponent characterId={characterId} />
    </View>
  );
};

export {Comics};
