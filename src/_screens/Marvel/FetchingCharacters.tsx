import React, {useEffect} from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';

import {listCharacters} from '@apis/marvelApi';
import {useState} from 'react';
import {CharactersResponse} from '@src/types/marvel';
import {CActivityIndicator} from '@components/CActivityIndicator';

const FetchingCharacters = () => {
  const [charactersResponse, setCharactersResponse] = useState<
    CharactersResponse
  >(undefined);

  useEffect(() => {
    const offset = 0;
    listCharacters(offset).then((result) => {
      setCharactersResponse(result);
    });
  }, []);

  const content = () => {
    if (charactersResponse) {
      return charactersResponse.data.results.map((item) => (
        <CText key={item.id} type="paragraph">
          {item.name}
        </CText>
      ));
    } else {
      return <CActivityIndicator />;
    }
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Fetching Characters</CTitle>
        <CText type="paragraph">
          See the source code for how to fetch characters. We have a problem
          though: when calling the endpoint there are 1493 characters but we
          only receive 20 of them. As you can see below, we don't get all the
          characters. We will solve this problem in the next screen.
        </CText>
      </CCard>

      <CCard>{content()}</CCard>
    </DeferredRender>
  );
};

export {FetchingCharacters};
