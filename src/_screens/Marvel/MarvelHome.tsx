import React from 'react';
import { CCard } from '@components/CCard';
import { CText } from '@components/CText';
import { CTitle } from '@components/CTitle';
import { DeferredRender } from '@components/DeferredRender';
import { CHighlight } from '@components/CHighlight';

const MarvelHome = () => {
    return (
        <DeferredRender>
            <CCard>
                <CTitle>What is the Marvel API?</CTitle>
                <CText type="paragraph">
                The Marvel API is a third-party API that allows fetching information
                    about characters in the Marvel Universe.
        </CText>
            </CCard>

            <CCard>
                <CTitle>Documentation</CTitle>
                <CHighlight>https://developer.marvel.com</CHighlight>
                <CText type="paragraph">
                    The API documentation is hard to view on a mobile device so it's best
                    to view this URL on your PC.
        </CText>
            </CCard>
            <CCard>
                <CTitle>What can we learn from this API?</CTitle>
                <CText type="normal">
                    This API is trickier than the Scryfall API. First, it uses authentication. So we need to learn how to use secrets without adding them into source code. Second, the results returned from the API are paginated. So we need a way to render items as we reach the end of the page.
        </CText>
            </CCard>
            
        </DeferredRender>
    );
};

export { MarvelHome };
