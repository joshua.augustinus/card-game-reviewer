import React from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import Config from 'react-native-config';
import {CHighlight} from '@components/CHighlight';

const publicKey = Config.MARVEL_PUBLIC_KEY;
const privateKey = Config.MARVEL_PRIVATE_KEY;

const getPublicKey = () => {
  if (publicKey) return publicKey.substring(0, 4) + '...';
  else return 'You .env file is missing MARVEL_PUBLIC_KEY';
};

const getPrivateKey = () => {
  if (privateKey) return privateKey.substring(0, 4) + '...';
  else return 'You .env file is missing MARVEL_PRIVATE_KEY';
};

const EnvFiles = () => {
  return (
    <DeferredRender>
      <CCard>
        <CTitle>Hiding Secrets</CTitle>
        <CText type="paragraph">
          We use <CHighlight>react-native-config</CHighlight> to store keys
          inside .env files. Then .env files can be added to .gitignore so it
          doesn't appear in source control.
        </CText>
        <CText type="normal">
          If you are running the app locally make sure you have read the README
          to setup the .env files.
        </CText>
        <CCard inner>
          <CText type="paragraph">MARVEL_PUBLIC_KEY: {getPublicKey()}...</CText>
          <CText type="normal">MARVEL_PRIVATE_KEY: {getPrivateKey()}...</CText>
        </CCard>

        <CText type="paragraph">
          Since the .env files aren't in source control you will also need a way
          to add them as part of your build pipeline. See the Confluence
          Documentation for a tutorial on how to do this with Visual Studio App
          Center.
        </CText>
      </CCard>
    </DeferredRender>
  );
};

export {EnvFiles};
