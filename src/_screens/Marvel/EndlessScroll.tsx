import React, {useEffect, useRef} from 'react';
import {CText} from '@components/CText';
import {StyleSheet, View} from 'react-native';
import {DeferredRender} from '@components/DeferredRender';
import {CCard} from '@components/CCard';
import {CTitle} from '@components/CTitle';
import {CButton} from '@components/CButton';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const EndlessScroll = () => {
  const navigation = useNavigation();
  const seeExample = () => {
    navigation.navigate('Endless Scroll Example');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Endless Scroll</CTitle>
        <CText type="paragraph">
          Now we will render out characters in a FlatList and use the prop
          "onEndReached" to define a function that will be called when we hit
          the end of the list. In this function we will fetch more characters
          from the API and then add it to the characters state.
        </CText>
        <CText type="paragraph">
          When rendering a list it's a good idea to make your renderItem
          function return a component wrapped in React.memo. This way it doesn't
          re-render unless the props passed to it changes.
        </CText>

        <CButton
          style={styles.exampleButton}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {EndlessScroll};
