import React from 'react';
import {CText} from '@components/CText';

import {CCard} from '@components/CCard';
import {CTitle} from '@components/CTitle';
import {CButton} from '@components/CButton';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import {useDispatch} from 'react-redux';
import {updateCharacterSelected} from '@src/reducers/marvelBuilder';
import {testCharacter} from './testCharacter';

import {DeferredRender} from '@components/DeferredRender';

const FetchingComics = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const seeExample = () => {
    dispatch(updateCharacterSelected(testCharacter));
    navigation.navigate('Comics');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Fetching Comics</CTitle>
        <CText type="paragraph">
          Fetching comics for a character brings the same issues as fetching
          characters. For some characters e.g. Captain America, they have so
          many comics that the results returned from the API are not complete
          and we must fetch more.
        </CText>
        <CText type="paragraph">
          There also seems to be a possible bug with duplicate comic IDs. I've
          found the issue with Captain America. In my source code I try and
          avoid the duplciate IDs by using a customId that is in the form
          [offset]_[id]. However, this should be fixed in the API and shouldn't
          be the job of the consumer.
        </CText>

        <CButton
          style={styles.exampleButton}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {FetchingComics};
