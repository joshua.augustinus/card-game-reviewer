import React from 'react';
import {CText} from '@components/CText';
import {CCard} from '@components/CCard';
import {CTitle} from '@components/CTitle';
import {ChartPopupComponent} from './ChartPopupComponent';
import styles from './styles';
import {View} from 'react-native';
import {testCharacter} from './testCharacter';

const ChartPopup = () => {
  return (
    <View style={styles.flex}>
      <CCard>
        <CTitle>Chart Popup</CTitle>
        <CText type="paragraph">
          Here is an example of a modal that shows a chart.
        </CText>
      </CCard>
      <View style={styles.flex}>
        <ChartPopupComponent character={testCharacter} />
      </View>
    </View>
  );
};

export {ChartPopup};
