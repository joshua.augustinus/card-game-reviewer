import React from 'react';
import {CText} from '@components/CText';
import {DeferredRender} from '@components/DeferredRender';
import {CCard} from '@components/CCard';
import {CTitle} from '@components/CTitle';
import {CButton} from '@components/CButton';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import {updateCharacterSelected} from '@src/reducers/marvelBuilder';
import {testCharacter} from './testCharacter';
import {useDispatch} from 'react-redux';

const CharacterDetails = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const seeExample = () => {
    dispatch(updateCharacterSelected(testCharacter));
    navigation.navigate('Character Info');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Character Details</CTitle>
        <CText type="paragraph">
          Next we need a way to drill down on a character and see detailed
          information on them. We could pass character information in params
          while performing a navigation action. However, I've found that doing
          so with Typescript is a little complicated because you need to define
          a RootParamList type.
        </CText>
        <CText type="paragraph">
          So instead of using navigation params we will be saving the character
          information in the redux state.
        </CText>

        <CButton
          style={styles.exampleButton}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {CharacterDetails};
