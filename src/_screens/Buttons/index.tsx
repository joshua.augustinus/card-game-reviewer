import React from 'react';
import {CButton} from '@src/_components/CButton';

import {CCard} from '../../_components/CCard';
import {useNavigation} from '@react-navigation/native';
import {DeferredRender} from '@components/DeferredRender';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {CPaperButton} from '@components/CPaperButton';
import {StyleSheet} from 'react-native';
import {CHighlight} from '@components/CHighlight';

const Buttons = () => {
  const navigation = useNavigation();
  const clickHandler = () => {};

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Buttons</CTitle>
        <CText type="paragraph">
          I added two component libraries to fasten development:{' '}
          <CHighlight>react-native-paper</CHighlight> and{' '}
          <CHighlight>react-native-elements</CHighlight>. You can see some
          example buttons below.
        </CText>
        <CHighlight>react-native-elements</CHighlight>
        <CButton title="Button Text" onPress={clickHandler}></CButton>
        <CButton
          style={styles.button}
          title="Button Text"
          small
          onPress={clickHandler}></CButton>
        <CText type="paragraph">
          These are buttons from react-native-elements that have been customised
        </CText>
        <CHighlight>react-native-paper</CHighlight>
        <CPaperButton textType="title" onPress={clickHandler}>
          Button Text
        </CPaperButton>
        <CPaperButton
          style={styles.button}
          textType="subHeading"
          onPress={clickHandler}>
          Button Text
        </CPaperButton>
        <CText type="paragraph">
          These are buttons from react-native-paper that have been customised
        </CText>
      </CCard>
    </DeferredRender>
  );
};

export {Buttons};

const styles = StyleSheet.create({
  button: {
    marginVertical: 1,
  },
});
