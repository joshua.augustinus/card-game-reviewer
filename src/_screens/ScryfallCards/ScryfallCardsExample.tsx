import React, {useState} from 'react';
import {PickerContainer} from './PickerContainer';
import {StyleSheet, View} from 'react-native';
import {Cards} from './Cards';
import {useSelector} from 'react-redux';
import {RootState} from '@src/types';
import {Modal} from 'react-native-paper';
import {CardImage} from './Cards/CardImage';

/**
 * The actual screen
 */
const ScryfallCardsExample = () => {
  const numCards = useSelector(
    (state: RootState) => state.scryfallState.cards?.length,
  );
  const [isModalVisisble, setIsModalVisible] = useState(false);
  const [modalCardUri, setModalCardUri] = useState(null);
  const hasCards = numCards > 0;

  /**
   * When modal is clicked the CardListItem will not rerender because we use
   * useCallback
   */
  const onCardPressed = React.useCallback((uri: string) => {
    setModalCardUri(uri);
    setIsModalVisible(true);
  }, []);
  return (
    <>
      <View style={{flex: 1}}>
        <Cards onCardPressed={onCardPressed} />
        <PickerContainer initialVisibility={!hasCards} />
      </View>
      <Modal
        contentContainerStyle={styles.modalContainer}
        visible={isModalVisisble}
        onDismiss={() => setIsModalVisible(false)}>
        <View style={styles.modalView}>
          <CardImage uri={modalCardUri} />
        </View>
      </Modal>
    </>
  );
};

export {ScryfallCardsExample};

const styles = StyleSheet.create({
  modalView: {backgroundColor: 'white', padding: 3, borderRadius: 10},
  modalContainer: {width: '75%', alignSelf: 'center'},
});
