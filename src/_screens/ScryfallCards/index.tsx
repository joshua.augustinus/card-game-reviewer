import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ScryfallCardsExample} from './ScryfallCardsExample';
import {ScryfallCardsHome} from './ScryfallCardsHome';

const Stack = createStackNavigator();

const ScryfallCards = (props: any) => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ScryfallCardsHome" component={ScryfallCardsHome} />
      <Stack.Screen
        name="ScryfallCardsExample"
        component={ScryfallCardsExample}
      />
    </Stack.Navigator>
  );
};

export {ScryfallCards};
