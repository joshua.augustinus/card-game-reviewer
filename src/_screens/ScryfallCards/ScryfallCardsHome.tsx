import React from 'react';
import {Card} from 'react-native-paper';
import {CCard} from '@components/CCard';
import {DeferredRender} from '@components/DeferredRender';
import {CTitle} from '@components/CTitle';
import {CText} from '@components/CText';
import {CButton} from '@components/CButton';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {CHighlight} from '@components/CHighlight';

const ScryfallCardsHome = () => {
  const navigation = useNavigation();

  const seeExample = () => {
    navigation.navigate('ScryfallCardsExample');
  };

  return (
    <DeferredRender>
      <CCard>
        <Card.Content>
          <CTitle>Cards</CTitle>
          <CText type="paragraph">
            After fetching sets from Scryfall, each set object in the response
            will have a property <CHighlight>search_uri</CHighlight>. This can
            be used to fetch cards for that set. An example search_uri value
            might be:
          </CText>
          <CText type="paragraph" color="accent">
            https://api.scryfall.com/cards/search?order=set&q=e%3Atsr&unique=prints
          </CText>
          <CText type="paragraph">
            After fetching from that url, we can feed that data into a FlatList
            to render images.
          </CText>
        </Card.Content>
      </CCard>
      <CCard>
        <Card.Content>
          <CTitle>Example</CTitle>
          <CText type="normal">
            This example will show cards as images after you select a set.
          </CText>

          <CButton
            style={styles.button}
            title="See Example"
            onPress={seeExample}></CButton>
        </Card.Content>
      </CCard>
    </DeferredRender>
  );
};

export {ScryfallCardsHome};

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
  },
});
