import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  cardContainer: {flex: 1, marginBottom: 1, marginRight: 1},
  flatListContainer: {paddingHorizontal: 20, paddingVertical: 10},
});

export default styles;
