import React from 'react';
import {Image} from 'react-native';

interface Props {
  uri: string;
}

const ASPECT_RATIO = 745 / 1040;

const CardImage = React.memo(function (props: Props) {
  return (
    <Image
      source={{
        uri: props.uri,
      }}
      style={{width: '100%', aspectRatio: ASPECT_RATIO}}
    />
  );
});

export {CardImage};
