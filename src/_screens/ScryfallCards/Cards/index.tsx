import React from 'react';

import {FlatList, View} from 'react-native';
import {CardListItem} from './CardListItem';
import {CardItem, RootState} from '@src/types';
import {useScreenInfo} from '@src/util/customHooks';
import {useSelector} from 'react-redux';

import styles from './styles';
import {CActivityIndicator} from '@components/CActivityIndicator';

const keyExtractor = (item: CardItem) => {
  return item.id;
};

interface Props {
  onCardPressed: (uri: string) => void;
}
/**
 * The actual screen
 */
const Cards = (props: Props) => {
  const isBusy = useSelector((state: RootState) => state.isBusy);

  const cardObjects = useSelector(
    (state: RootState) => state.scryfallState.cards,
  );
  const screenInfo = useScreenInfo();
  const columns = screenInfo.isSmallScreen ? 1 : 2;

  const itemRender = ({item, index}: {item: CardItem; index: number}) => {
    //If number of columns is 1 then don't bother doing anything when pressed
    const cardPressedHandler =
      columns > 1 ? props.onCardPressed : (uri: string) => {};
    return (
      <CardListItem item={item} index={index} onPress={cardPressedHandler} />
    );
  };

  if (isBusy) {
    return <CActivityIndicator />;
  } else if (cardObjects?.length > 0) {
    return (
      <View style={styles.flatListContainer}>
        <FlatList
          windowSize={8}
          numColumns={columns}
          maxToRenderPerBatch={4}
          data={cardObjects}
          renderItem={itemRender}
          keyExtractor={keyExtractor}
        />
      </View>
    );
  } else {
    return null;
  }
};

export {Cards};
