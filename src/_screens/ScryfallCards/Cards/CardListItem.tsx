import React from 'react';

import {CardItem} from '@src/types';
import {Image, View} from 'react-native';
import styles from './styles';
import {CardImage} from './CardImage';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Props {
  item: CardItem;
  index: number;
  onPress: (uri: string) => void;
}

const CardListItem = React.memo(function (props: Props) {
  //You can check that this doesn't rerender when modal shows
  //by uncommenting line below
  //console.log('CardListItem rendered', props.item.id);
  let uri: string;

  //The actual location of the image uri can be in one of two places
  //This is based on Scryfall API which I don't control
  if (props?.item?.image_uris?.normal) {
    uri = props.item.image_uris.normal;
  } else if (props?.item?.card_faces?.length > 0) {
    uri = props.item.card_faces[0].image_uris.normal;
  }

  if (uri) {
    return (
      <View style={styles.cardContainer}>
        <TouchableOpacity onPress={() => props.onPress(uri)} activeOpacity={1}>
          <CardImage uri={uri} />
        </TouchableOpacity>
      </View>
    );
  } else {
    return null;
  }
});

export {CardListItem};
