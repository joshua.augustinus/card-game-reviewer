import {CColors} from '@src/colors';
import {elevations} from '@src/util/elevations';
import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  iconButton: {
    backgroundColor: CColors.button.primary,
  },
  pickerCaret: {
    position: 'absolute',
    right: 10,

    elevation: elevations.HIGH,
    backgroundColor: CColors.button.primary,
  },
  chevronRightContainer: {
    position: 'absolute',
    left: -15,
    top: 0,
    opacity: 0,
    elevation: elevations.MEDIUM,
  },
  pickerContainer: {
    position: 'absolute',
    width: '100%',
    elevation: elevations.MEDIUM,
  },
  pickerCard: {
    backgroundColor: CColors.button.primary,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  picker: {height: 50, width: '100%', color: CColors.buttonText.primary},
});

export default styles;
