import React from 'react';
import {CButton} from '@src/_components/CButton';
import {StyleSheet} from 'react-native';
import {CCard} from '../../_components/CCard';
import {useNavigation} from '@react-navigation/native';
import {DeferredRender} from '@components/DeferredRender';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';

const DrawerNavigatorProblems = () => {
  const navigation = useNavigation();
  const seeExample = () => {
    navigation.navigate('Login Example');
  };

  const seeForgetfulExample = () => {
    navigation.navigate('Forgetful View Example');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Drawer Navigator</CTitle>
        <CText type="paragraph">
          In this example we shall navigate to a login form that exists as a
          screen inside the DrawerNavigator.
        </CText>
        <CText type="paragraph">
          Type something inside the login form then press back then press the
          button again to return to the login form. You will see that the text
          you entered is still there. This means that pressing the back button
          when using DrawerNavigator keeps the component mounted.
        </CText>

        <CButton
          small
          style={styles.button}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
      <CCard>
        <CTitle>ForgetfulView component</CTitle>
        <CText type="paragraph" color="accent">
          src/_components/ForgetfulView
        </CText>
        <CText type="paragraph">
          In some situations you will want components to unmount after
          navigating away. The ForgetfulView component I created will unmount
          children when a user navigates away.
        </CText>
        <CText type="paragraph">
          You can test this on the next form by typing something then returning
          then going back to the form. The text you typed will be gone on
          returning.
        </CText>

        <CButton
          small
          style={styles.button}
          title="See Forgetful View Example"
          onPress={seeForgetfulExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {DrawerNavigatorProblems};

const styles = StyleSheet.create({
  button: {
    marginBottom: 5,
  },
});
