import React from 'react';
import {ForgetfulView} from '@components/ForgetfulView';
import {LoginExample} from '@screens/Login/LoginExample';

const ForgetfulViewExample = () => {
  return (
    <ForgetfulView>
      <LoginExample />
    </ForgetfulView>
  );
};

export {ForgetfulViewExample};
