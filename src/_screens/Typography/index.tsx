import React from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import {SlidingView} from '@components/SlidingView';
import {ScrollView} from 'react-native-gesture-handler';

const FONTS = [
  'NunitoSans-Black',
  'NunitoSans-BlackItalic',
  'NunitoSans-BlackBold',
  'NunitoSans-Bold',
  'NunitoSans-BoldItalic',
  'NunitoSans-ExtraBold',
  'NunitoSans-ExtraBoldItalic',

  'NunitoSans-ExtraLight',
  'NunitoSans-ExtraLightItalic',
  'NunitoSans-Italic',
  'NunitoSans-Light',
  'NunitoSans-LightItalic',
  'NunitoSans-Regular',
  'NunitoSans-SemiBold',
  'NunitoSans-SemiBoldItalic',
  'Satisfy-Regular',
];

const SYSTEM_FONTS = [
  'sans-serif',
  'sans-serif-light',
  'sans-serif-thin',
  'sans-serif-condensed',
  'sans-serif-medium',
  'sans-serif-black',
  'serif',
  'monospace',
  'serif-monospace',
  'casual',
  'cursive',
  'sans-serif-smallcaps',
];

const Typography = () => {
  return (
    <ScrollView>
      <DeferredRender>
        <CCard>
          <CTitle>Typography</CTitle>
          <CText type="normal">
            I've added some Google fonts to this project. Adding fonts is tricky
            so I've included a tutorial in the Confluence Documentation.
          </CText>
        </CCard>
      </DeferredRender>
      <DeferredRender>
        <CCard>
          <CTitle>Downloaded Fonts</CTitle>
          {FONTS.map((item, index) => (
            <SlidingView
              key={item}
              toValue={0}
              startValue={-800}
              delay={index * 200}>
              <CText type="subHeading" fontFamily={item}>
                {item}
              </CText>
            </SlidingView>
          ))}
        </CCard>
      </DeferredRender>
      <DeferredRender>
        <CCard>
          <CTitle>System Fonts</CTitle>
          <CText type="paragraph">
            These fonts should be accessible by default.
          </CText>
          {SYSTEM_FONTS.map((item) => (
            <CText key={item} type="subHeading" fontFamily={item}>
              {item}
            </CText>
          ))}
        </CCard>
      </DeferredRender>
    </ScrollView>
  );
};

export {Typography};
