import React, {useEffect, useRef, useState} from 'react';
import {RootState, SetItem} from '@src/types';
import {useDispatch, useSelector} from 'react-redux';
import * as Animatable from 'react-native-animatable';
import {Picker} from '@react-native-community/picker';
import {IconButton} from 'react-native-paper';
import {CColors} from '@src/colors';
import styles from './styles';
import {bounceOutLeft} from '@src/util/customAnimations';
import {CIconButton} from '@components/CIconButton';
import {View} from 'react-native';

const PICKER_DEFAULT_KEY = '0';

interface Props {
  initialVisibility: boolean;
}

const getDefaultPickerItem = () => {
  return (
    <Picker.Item
      key={PICKER_DEFAULT_KEY}
      value={PICKER_DEFAULT_KEY}
      label="Please select a set..."
    />
  );
};

/**
 * This shows a picker that can animate in and out
 */
const PickerContainer = (props: Props) => {
  const setJson: SetItem[] = useSelector(
    (state: RootState) => state.scryfallState.sets,
  );
  const pickerRef = useRef(null);
  const chevronRightRef = useRef(null);
  const dispatch = useDispatch();
  const [selectedValue, setSelectedValue] = useState('');
  const [isVisible, setIsVisible] = useState(props.initialVisibility);

  useEffect(() => {
    if (!isVisible) {
      chevronRightRef.current.fadeInLeft();
    }
  }, [isVisible]);

  const setSelected = (itemValue: string, itemIndex: number) => {
    if (itemValue === PICKER_DEFAULT_KEY) return;
    const uri = itemValue.toString();
    setSelectedValue(uri);
  };

  if (isVisible)
    return (
      <Animatable.View
        ref={pickerRef}
        animation="bounceInLeft"
        style={styles.pickerContainer}>
        <View style={styles.pickerCard}>
          <Picker
            selectedValue={selectedValue}
            style={styles.picker}
            onValueChange={setSelected}>
            {getDefaultPickerItem()}
            {setJson.map((item) => {
              return (
                <Picker.Item
                  key={item.id}
                  label={item.name}
                  value={item.search_uri}
                />
              );
            })}
          </Picker>
          <IconButton
            style={styles.pickerCaret}
            icon="chevron-left"
            color={CColors.buttonText.primary}
            size={25}
            onPress={() => {
              pickerRef.current.animate(bounceOutLeft).then(() => {
                setIsVisible(false);
              });
            }}
          />
        </View>
      </Animatable.View>
    );
  else
    return (
      <Animatable.View
        style={styles.chevronRightContainer}
        ref={chevronRightRef}>
        <CIconButton
          flatLeft
          iconName="chevron-right"
          onPress={() => {
            setIsVisible(true);
          }}
        />
      </Animatable.View>
    );
};

export {PickerContainer};
