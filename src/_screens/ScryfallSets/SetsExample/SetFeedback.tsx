import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {View} from 'react-native';
import {StyleSheet} from 'react-native';
import {CText} from '@components/CText';

const Stack = createStackNavigator();

/**
 * The actual screen
 */
const SetFeedback = () => {
  /** 
   * You MUST place absolute position buttons at the end *
      https://stackoverflow.com/questions/36938742/touchablehighlight-not-clickable-if-position-absolute
   **/

  return (
    <View style={styles.container}>
      <CText type="normal" style={styles.textCenter}>
        This is a placeholder. We will replace this with a Card gallery in a
        future example.
      </CText>
    </View>
  );
};

export {SetFeedback};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
  },
  textCenter: {
    textAlign: 'center',
  },
});
