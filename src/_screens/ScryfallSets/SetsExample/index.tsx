import React from 'react';
import {PickerContainer} from './PickerContainer';
import {StyleSheet, View} from 'react-native';
import {SetFeedback} from './SetFeedback';

/**
 * The actual screen
 */
const SetsExample = () => {
  /** 
   * You MUST place absolute position buttons at the end *
      https://stackoverflow.com/questions/36938742/touchablehighlight-not-clickable-if-position-absolute
   **/

  return (
    <>
      <View
        style={{
          flex: 1,
        }}>
        <SetFeedback />
        <PickerContainer initialVisibility={true} />
      </View>
    </>
  );
};

export {SetsExample};


