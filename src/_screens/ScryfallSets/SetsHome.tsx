import React from 'react';
import {Card} from 'react-native-paper';
import {CCard} from '@components/CCard';
import {DeferredRender} from '@components/DeferredRender';
import {CTitle} from '@components/CTitle';
import {CText} from '@components/CText';
import {CButton} from '@components/CButton';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {CHighlight} from '@components/CHighlight';

const SetsHome = () => {
  const navigation = useNavigation();

  const seeExample = () => {
    navigation.navigate('SetsExample');
  };

  return (
    <DeferredRender>
      <CCard>
        <Card.Content>
          <CTitle>Sets</CTitle>
          <CText type="paragraph">
            To fetch all the sets, call the following endpoint:
          </CText>
          <CHighlight>https://api.scryfall.com/sets</CHighlight>
        </Card.Content>
      </CCard>
      <CCard>
        <Card.Content>
          <CTitle>Example</CTitle>
          <CText type="normal">
            This example will show the list of sets inside a Picker
          </CText>

          <CButton
            style={styles.button}
            title="See Example"
            onPress={seeExample}></CButton>
        </Card.Content>
      </CCard>
    </DeferredRender>
  );
};

export {SetsHome};

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
  },
});
