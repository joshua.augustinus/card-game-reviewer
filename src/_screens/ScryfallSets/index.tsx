import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SetsHome} from './SetsHome';
import {SetsExample} from './SetsExample';

const Stack = createStackNavigator();

const ScryfallSets = (props: any) => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SetsHome" component={SetsHome} />
      <Stack.Screen name="SetsExample" component={SetsExample} />
    </Stack.Navigator>
  );
};

export {ScryfallSets};
