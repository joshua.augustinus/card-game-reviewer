import React from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import {ShareConfluence} from '@components/ShareConfluence';
import {ScrollView} from 'react-native-gesture-handler';

const Introduction = () => {
  return (
    <ScrollView>
      <DeferredRender delay={200}>
        <CCard>
          <CTitle>Introduction</CTitle>
          <CText type="paragraph">
            The target audience of this application is people who are learning
            React Native and want a sample application with open source code.
          </CText>
          <CText type="normal">
            It is assumed you already know the basics of React Native and Redux
            and want to learn more complicated concepts.
          </CText>
        </CCard>
      </DeferredRender>
      <DeferredRender delay={600}>
        <CCard>
          <CTitle>Confluence Documentation</CTitle>
          <CText type="paragraph">
            This application alone is not a tutorial. You will need to read the
            source code to see what's going on.
          </CText>
          <CText type="paragraph">
            The Confluence Documentation has links to the source code as well as
            other useful information. Press the button below and email yourself
            the documentation link.
          </CText>
          <ShareConfluence />
        </CCard>
      </DeferredRender>
      <DeferredRender delay={1000}>
        <CCard>
          <CTitle>Navigating</CTitle>
          <CText type="normal">
            Press the menu icon in top left to open the side drawer. The topics
            you can see will show you examples of various components that you
            may want to put in your React Native app.
          </CText>
        </CCard>
      </DeferredRender>
    </ScrollView>
  );
};

export {Introduction};
