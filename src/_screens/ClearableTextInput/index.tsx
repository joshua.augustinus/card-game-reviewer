import React, {useState} from 'react';
import {View} from 'react-native';
import {Title} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import {CCard} from '@components/CCard';
import {useNavigation} from '@react-navigation/native';
import {CTextInput} from '@components/CTextInput';
import {CText} from '@components/CText';

const ClearableTextInput = () => {
  const [text, setText] = useState('');

  return (
    <View>
      <CCard>
        <Title style={styles.title}>Clearable Text Input</Title>
        <CText type="paragraph" color="accent">
          src/_components/CInputText
        </CText>
        <CText type="paragraph">
          This is an example component that allows clearing of the text input by
          pressing the right icon.
        </CText>
        <CTextInput
          clearable
          style={styles.input}
          label="Search Filter"
          value={text}
          onChangeText={(text: string) => setText(text)}
        />
      </CCard>
    </View>
  );
};

export {ClearableTextInput};

const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
  },
  loginButton: {
    margin: 20,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 10,
  },
  forgotPassword: {
    marginTop: 5,
    alignSelf: 'flex-end',
    textDecorationLine: 'underline',
  },
});
