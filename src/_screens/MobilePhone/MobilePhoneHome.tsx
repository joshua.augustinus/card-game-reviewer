import React from 'react';
import {CButton} from '@src/_components/CButton';
import {CCard} from '../../_components/CCard';
import {useNavigation} from '@react-navigation/native';
import {DeferredRender} from '@components/DeferredRender';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import styles from './styles';
import {CHighlight} from '@components/CHighlight';

const MobilePhoneHome = () => {
  const navigation = useNavigation();
  const seeExample = () => {
    navigation.navigate('MobilePhoneExample');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Mobile Phone Input</CTitle>
        <CText type="paragraph">
          You may want a form for users to input their mobile phone. The
          following example is a fake form with a mobile phone input.
        </CText>

        <CButton title="See Example" onPress={seeExample}></CButton>
      </CCard>
      <CCard>
        <CTitle>Package Dependencies</CTitle>

        <CHighlight>react-native-country-picker-modal</CHighlight>
        <CText type="paragraph">
          I'm using this package to handle selection of country and also convert
          country code to a mobile number prefix.
        </CText>
        <CHighlight>react-native-localize</CHighlight>
        <CText type="paragraph">
          I'm using this package to figure out the current country of the user
          based on device information.
        </CText>
      </CCard>
    </DeferredRender>
  );
};

export {MobilePhoneHome};
