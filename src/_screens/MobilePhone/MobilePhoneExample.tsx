import React, {useState, useEffect} from 'react';

import {View, Text} from 'react-native';
import {TextInput} from 'react-native-paper';
import CountryPicker, {
  Country,
  CountryCode,
} from 'react-native-country-picker-modal';
import {CCard} from '../../_components/CCard';
import {CButton} from '../../_components/CButton';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import {getCountryCallingCodeAsync} from 'react-native-country-picker-modal/lib/CountryService';
import * as RNLocalize from 'react-native-localize';
import {FormDisclaimer} from '@components/FormDisclaimer';

const getCountryCode = () => {
  const locales = RNLocalize.getLocales();
  console.log(locales[0]);
  const result = locales[0].countryCode as CountryCode;
  return result;
};

const MobilePhoneExample = () => {
  const navigation = useNavigation();
  const [phone, setPhone] = useState('');
  const [callingCode, setCallingCode] = useState('');
  const [cca2, setCca2] = useState<CountryCode>(getCountryCode());

  const selectCountry = (country: Country) => {
    setCca2(country.cca2);
    setCallingCode('+' + country.callingCode.toString());
  };

  const onVerify = async () => {};

  useEffect(() => {
    getCountryCallingCodeAsync(cca2).then((callingCode) => {
      setCallingCode('+' + callingCode);
    });
  }, []);

  return (
    <View>
      <CCard>
        <Text>
          Please enter your mobile phone number below to verify with an SMS
          code.
        </Text>
        <View style={styles.mobileRow}>
          <CountryPicker
            containerButtonStyle={{marginBottom: 10}}
            onSelect={(value) => selectCountry(value)}
            translation="common"
            countryCode={cca2}
          />
          <Text style={{width: 40, marginBottom: 15}}>{callingCode}</Text>
          <TextInput
            keyboardType="phone-pad"
            style={{flex: 1}}
            textContentType="telephoneNumber"
            label="Mobile Phone Number"
            value={phone}
            onChangeText={(text) => setPhone(text)}
          />
        </View>
        <CButton title="Verify" onPress={onVerify} />
      </CCard>
      <FormDisclaimer />
    </View>
  );
};

export {MobilePhoneExample};
