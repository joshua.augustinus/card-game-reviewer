import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MobilePhoneExample} from './MobilePhoneExample';
import {MobilePhoneHome} from './MobilePhoneHome';

const Stack = createStackNavigator();

const MobilePhone = (props: any) => {
  return (
    <Stack.Navigator
      initialRouteName="MobilePhoneHome"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="MobilePhoneHome" component={MobilePhoneHome} />
      <Stack.Screen name="MobilePhoneExample" component={MobilePhoneExample} />
    </Stack.Navigator>
  );
};

export {MobilePhone};
