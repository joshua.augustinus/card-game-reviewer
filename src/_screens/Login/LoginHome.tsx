import React from 'react';
import {CButton} from '@src/_components/CButton';
import {StyleSheet} from 'react-native';
import {CCard} from '../../_components/CCard';
import {useNavigation} from '@react-navigation/native';
import {DeferredRender} from '@components/DeferredRender';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';

const LoginHome = () => {
  const navigation = useNavigation();
  const seeExample = () => {
    navigation.navigate('LoginExample');
  };

  return (
    <DeferredRender>
      <CCard>
        <CTitle>Login Form</CTitle>
        <CText type="paragraph">
          The following example shows a fake Login Form. You should view the
          source code to get further details.
        </CText>
        <CText type="paragraph">
          You will notice that if we type something inside the login form and
          press back then press See Example and return to the login form the
          text we entered is gone. That's because we are using a StackNavigator
          which pops off the last screen when we hit back. We will see later
          that this doesn't occur for the DrawerNavigator.
        </CText>
        <CButton
          style={styles.button}
          title="See Example"
          onPress={seeExample}></CButton>
      </CCard>
    </DeferredRender>
  );
};

export {LoginHome};

const styles = StyleSheet.create({
  button: {
    marginVertical: 20,
  },
});
