import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginExample} from './LoginExample';
import {LoginHome} from './LoginHome';

const Stack = createStackNavigator();

const Login = (props: any) => {
  return (
    <Stack.Navigator
      initialRouteName="LoginHome"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="LoginHome" component={LoginHome} />
      <Stack.Screen name="LoginExample" component={LoginExample} />
    </Stack.Navigator>
  );
};

export {Login};
