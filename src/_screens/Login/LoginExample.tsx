import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {CButton} from '@src/_components/CButton';
import {TextInput, Title} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import {CPassword} from '../../_components/CPassword';
import {CCard} from '../../_components/CCard';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {FormDisclaimer} from '@components/FormDisclaimer';

const LoginExample = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loginHandler = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'Sets'}],
    });
  };

  return (
    <View>
      <CCard>
        <Title style={styles.title}>Sign In</Title>
        <TextInput
          style={styles.input}
          label="Email Address"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <CPassword
          style={styles.input}
          label="Password"
          value={password}
          onChangeText={(text) => setPassword(text)}
        />

        <CButton
          title="Login"
          onPress={loginHandler}
          style={styles.loginButton}
        />
        <TouchableOpacity>
          <Text style={styles.forgotPassword}>Forgot password?</Text>
        </TouchableOpacity>
      </CCard>
      <FormDisclaimer />
    </View>
  );
};

export {LoginExample};

const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
  },
  loginButton: {
    margin: 20,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 10,
  },
  forgotPassword: {
    marginTop: 5,
    alignSelf: 'flex-end',
    textDecorationLine: 'underline',
  },
});
