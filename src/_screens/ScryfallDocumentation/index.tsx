import React from 'react';
import {CCard} from '@components/CCard';
import {CText} from '@components/CText';
import {CTitle} from '@components/CTitle';
import {DeferredRender} from '@components/DeferredRender';
import {CHighlight} from '@components/CHighlight';

const ScryfallDocumentation = () => {
  return (
    <DeferredRender>
      <CCard>
        <CTitle>What is Scryfall API?</CTitle>
        <CText type="paragraph">
          Scryfall API is a third-party API that allows fetching information
          about Magic the Gathering sets and cards. It allows us to learn how to
          fetch data from an API without having to make an API ourselves. It
          doesn't require any authentication. It also gives us image URLs which
          allows us to learn how to render images in a list effectively.
        </CText>
      </CCard>

      <CCard>
        <CTitle>Documentation</CTitle>
        <CHighlight>https://scryfall.com/docs/api</CHighlight>
        <CText type="paragraph">
          The API documentation is hard to view on a mobile device so it's best
          to view this URL on your PC.
        </CText>
      </CCard>
      <CCard>
        <CTitle>Postman</CTitle>
        <CText type="normal">
          One way to examine the response from an API is to use Postman. Postman
          is a tool you can download onto your PC that allows you to make calls
          to APIs.
        </CText>
      </CCard>
      <CCard>
        <CTitle>Using fetch</CTitle>
        <CText type="normal">
          See <CHighlight>src/apis</CHighlight> for example code on fetching
          from an API.
        </CText>
      </CCard>
    </DeferredRender>
  );
};

export {ScryfallDocumentation};
