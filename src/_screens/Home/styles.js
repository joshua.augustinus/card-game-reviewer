import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },
  flex_1: {
    flex: 1,
    justifyContent: 'center',
  },
  flex_2: {
    flex: 2,
  },
  image:{
    flex: 1,
    justifyContent: 'center',
    borderRadius:10
  },
  svgWrapper: {
    flex: 2,
    aspectRatio: 1,
  },
  text: {
    textAlign: 'center',
    marginBottom: 10,
  },
  textHeading: {
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  bottomFloat: {
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    marginBottom: 50,
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 15,
    width: '80%',
  },
  bottomView: {
    height: 140,
    backgroundColor: 'red',
  },
});

export default styles;
