import React from 'react';
import Svg, {Ellipse} from 'react-native-svg';
import {useScreenInfo} from '@src/util/customHooks';
import {StyleSheet} from 'react-native';
import {SlidingView} from '@components/SlidingView';
import {ScalingView} from '@components/ScalingView';

const EllipseBackground = (props: any) => {
  const screenInfo = useScreenInfo();
  return (
    <ScalingView
      duration={1000}
      delay={500}
      toValue={1}
      startValue={5}
      style={{
        ...styles.container,
        width: screenInfo.width,
      }}>
      <Svg height="100%" width="100%" viewBox="0 0 100 100">
        <Ellipse cx="50" cy="50" rx="90" ry="40" fill="white" />
      </Svg>
    </ScalingView>
  );
};

export {EllipseBackground};

const styles = StyleSheet.create({
  container: {
    aspectRatio: 1,
    position: 'absolute',
    top: -300,

    height: 600,
  },
});
