import React from 'react';
import {View, Image} from 'react-native';

import styles from './styles';

import {HomeScreenData} from '@src/types';
import {CText} from '@src/_components/CText';
import {FadingView} from '@components/FadingView';

interface Props {
  data: HomeScreenData;
}

const SwipableScreen = (props: Props) => {
  return (
    <View style={styles.container}>
      <FadingView
        duration={700}
        delay={1400}
        toValue={1}
        startValue={0}
        style={styles.flex_2}>
        <Image
          style={styles.image}
          resizeMode="contain"
          source={props.data.imageSource}
        />
      </FadingView>
      <View style={styles.flex_1}>
        <CText color="light" type="title" style={styles.textHeading}>
          {props.data.heading}
        </CText>
      </View>
      <View style={styles.flex_2}>
        {props.data.content.map((value, index) => {
          return (
            <CText
              color="light"
              type="subHeading"
              key={index}
              style={styles.text}>
              {value}
            </CText>
          );
        })}
      </View>
      <View style={styles.bottomView} />
    </View>
  );
};

export {SwipableScreen};
