import React from 'react';
import {View, ColorValue} from 'react-native';
import Swiper from 'react-native-swiper';
import styles from './styles';
import {CButton} from '@components/CButton';
import {SwipableScreen} from './SwipableScreen';
import {CColors} from '@src/colors';
import {useDispatch} from 'react-redux';
import {updateContainerType} from '@src/reducers';
import {EllipseBackground} from './EllipseBackground';
import {FadingView} from '@components/FadingView';

const dot = (color: ColorValue) => {
  return (
    <View
      style={{
        backgroundColor: color,
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
      }}
    />
  );
};

const Home = () => {
  const dispatch = useDispatch();

  const screen1 = {
    heading: 'Who is this app for?',
    content: [
      'This app is for people learning React Native. It assumes you know the basics but want a sample app to learn more complicated things.',
      'The code is open source and you can use ideas from it in your own apps.',
      'Swipe left to read more info or press Start to skip all that.',
    ],

    imageSource: require('../../images/marvel1.jpg'),
  };

  const screen2 = {
    heading: 'Marvel & Scryfall API',
    content: [
      'This app connects to two third-party APIs: Marvel and Scryfall.',
      'This allows us to learn how to connect to an API without having to build one ourselves.',
    ],

    imageSource: require('../../images/thor.png'),
  };

  const screen3 = {
    heading: 'Why Scryfall API?',
    content: [
      'The Scryfall API gives us information about Magic the Gathering cards.',
      'It provides image links so we can learn how to render images and deal with large lists.',
    ],
    imageSource: require('../../images/magic2.png'),
  };

  const screen4 = {
    heading: 'Why Marvel API?',
    content: [
      'The Marvel API is more complex because it requires authentication.',
      'However, it is a good API to learn about pagination because when fetching data they often limit the amount of results.',
    ],
    imageSource: require('../../images/marvel2.jpg'),
  };

  const screen5 = {
    heading: 'What is inside this app?',
    content: [
      'This app has various screens and components that may be useful in your own apps. ',
      'It uses some other component libraries like react-native-paper.',
    ],

    imageSource: require('../../images/magic4.png'),
  };

  const signupHandler = () => {
    dispatch(updateContainerType('Drawer'));
  };

  return (
    <View style={{flex: 1, backgroundColor: CColors.primary}}>
      <EllipseBackground />
      <Swiper
        dot={dot(CColors.background)}
        activeDot={dot(CColors.button.primary)}
        style={styles.wrapper}
        showsButtons={false}
        loop={false}>
        <SwipableScreen data={screen1} />
        <SwipableScreen data={screen2} />
        <SwipableScreen data={screen3} />
        <SwipableScreen data={screen4} />
        <SwipableScreen data={screen5} />
      </Swiper>
      <FadingView
        duration={700}
        delay={1000}
        toValue={1}
        startValue={0}
        style={styles.bottomFloat}>
        <CButton title="Start" style={styles.button} onPress={signupHandler} />
      </FadingView>
    </View>
  );
};

export {Home};
