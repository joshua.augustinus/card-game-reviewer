export interface HttpResponse {
  isSuccess: boolean;
  status: number;
  data: any;
}

