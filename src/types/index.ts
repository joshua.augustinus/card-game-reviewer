import {ComponentType, ReactNode} from 'react';
import {ImageSourcePropType} from 'react-native';
import {MarvelState} from './marvel';

export type ButtonType = undefined | 'secondary' | 'light' | 'primary';

export interface HomeScreenData {
  content: string[];
  heading: string;
  imageSource: ImageSourcePropType;
}

export interface SetItem {
  object: string;
  id: string;
  code: string;
  name: string;
  released_at: string;
  set_type: string;
  card_count: number;
  digital: boolean;
  icon_svg_uri: string;
  search_uri: string;
}

export interface CardItem {
  object: string;
  id: string;
  name: string;
  /**
   * Url of a normal sized images
   */
  image_uris: ImageUris;
  card_faces: CardFace[];
}

export interface CardFace {
  name: string;
  image_uris: ImageUris;
}

export interface ImageUris {
  normal: string;
  large: string;
  png: string;
}

export interface RootState {
  appHeaderTitle: string;
  isBusy: boolean;
  containerType: ContainerType;
  scryfallState: ScryfallState;
  marvelState: MarvelState;
}

export interface ScryfallState {
  cards: CardItem[];
  cardsUri: string;
  sets: SetItem[];
}

export type TextType =
  | 'normal'
  | 'subHeading'
  | 'title'
  | 'paragraph'
  | 'small';
export type ColorType =
  | 'default'
  | 'secondary'
  | 'light'
  | 'primary'
  | 'accent';
export type ContainerType =
  | 'Stack'
  | 'Drawer'
  | 'PaperBottomNav'
  | 'RnBottomNav';

export interface DrawerItem extends ScreenInfo {
  /**
   * The label that appears on the sidebar
   */
  label: string;

  hasDivider?: boolean;
}

export type ActivityIndicatorType = 'absolute' | 'small' | 'default';

export interface ScreenInfo {
  /**
   * The label that appears in the header and also the nav route
   */
  screenName: string;
  component: any;
}
