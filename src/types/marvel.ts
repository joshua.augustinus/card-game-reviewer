export interface CharactersResponse {
  status: string;
  code: number; //200
  data: CharactersData;
  filter: string;
}

export interface ComicsResponse {
  status: string;
  code: number;
  data: ComicsData;
}

export interface ComicsData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Comic[];
}

export interface CharactersData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Character[];
}

export interface Character {
  id: number;
  name: string;
  description: string;
  comics: Collection;
  series: Collection;
  stories: Collection;
  events: Collection;
}

export interface Collection {
  available: number;
}

export interface ComicCollection {
  available: number;
  items: Comic[];
}

export interface Comic {
  id: number;
  title: string;
  customId: string;
}

export interface MarvelState {
  characterSelected: Character;
}
