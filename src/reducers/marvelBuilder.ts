import {ActionReducerMapBuilder, createAction} from '@reduxjs/toolkit';
import {RootState} from '@src/types';
import {Character} from '@src/types/marvel';

export const updateCharacterSelected = createAction<Character>(
  'marvelCharacter/update',
);

/**
 * Function to add handlers for magic reducer actions
 */
export const addMarvelHandlers = (
  builder: ActionReducerMapBuilder<RootState>,
) => {
  builder.addCase(updateCharacterSelected, (state, action) => {
    state.marvelState.characterSelected = action.payload;
  });
};
