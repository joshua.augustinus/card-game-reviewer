import {createAction, createReducer} from '@reduxjs/toolkit';
import {RootState, ContainerType} from '@src/types';
import {addMarvelHandlers} from './marvelBuilder';
import {addMagicHandlers} from './scryfallBuilder';

/**
 * This is the modern way of creating reducers using redux-toolkit
 * https://redux-toolkit.js.org/api/createReducer
 */

/**
 * If you call updateAppHeaderTitle("NewTitle") it will return an action:
 * {type:'appHeaderTitle/update', payload:"NewTitle"}
 */
export const updateAppHeaderTitle = createAction<string>(
  'appHeaderTitle/update',
);
export const updateIsBusy = createAction<boolean>('isBusy/update');

export const updateContainerType = createAction<ContainerType>(
  'containerType/update',
);
export const deleteState = createAction('state/delete');

/**
 * Initial State
 */
const initialState: RootState = {
  appHeaderTitle: '',
  scryfallState: {
    cards: [],
    cardsUri: null,
    sets: [],
  },

  marvelState: {
    characterSelected: undefined,
  },

  isBusy: false,
  containerType: 'Stack',
};

/**
 * createReducer uses 'immer' to let you write reducers and allow you to mutate state
 * directly without having to use  ... spread operator.
 * https://redux-toolkit.js.org/api/createReducer
 */
const rootReducer = createReducer(initialState, (builder) => {
  builder.addCase(deleteState, (state, action) => {
    return initialState;
  });

  builder.addCase(updateAppHeaderTitle, (state, action) => {
    state.appHeaderTitle = action.payload;
  });

  builder.addCase(updateIsBusy, (state, action) => {
    state.isBusy = action.payload;
  });

  builder.addCase(updateContainerType, (state, action) => {
    state.containerType = action.payload;
  });

  addMagicHandlers(builder);
  addMarvelHandlers(builder);
});

export default rootReducer;
