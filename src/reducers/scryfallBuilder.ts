import {ActionReducerMapBuilder, createAction} from '@reduxjs/toolkit';
import {CardItem, RootState, SetItem} from '@src/types';

export const updateSets = createAction<SetItem[]>('sets/update');
export const updateCards = createAction<CardItem[]>('cards/update');
export const updateCardsUri = createAction<string>('cardsUri/update');

/**
 * Function to add handlers for magic reducer actions
 */
export const addMagicHandlers = (
  builder: ActionReducerMapBuilder<RootState>,
) => {
  builder.addCase(updateCards, (state, action) => {
    state.scryfallState.cards = action.payload;
  });

  builder.addCase(updateCardsUri, (state, action) => {
    state.scryfallState.cards = null;
    state.scryfallState.cardsUri = action.payload;
  });

  builder.addCase(updateSets, (state, action) => {
    state.scryfallState.sets = action.payload;
  });
};
