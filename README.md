### Setup
1. Go to https://developer.marvel.com/ and get a public and private key
2. Create a **.env.debug** file inside root with the following contents:
```
ENVIRONMENT = DEBUG
MARVEL_PUBLIC_KEY = 12345
MARVEL_PRIVATE_KEY = 12345
```
Note: Replace 12345 with your private/public keys

### Android only
While react native is cross-platform I haven't fully tested the app yet on IOS since I haven't setup my Mac environment yet. All the code is react native but there are some native components:
- Splash Screen
- Fonts

### Debugging
Download packages:
```
yarn
```
Start Metro server:
```
yarn start
```
Build and deploy to android device:
```
yarn debug
```

### Confluence Documentation
[View documentation](https://jauggy.atlassian.net/wiki/spaces/RNDWS/overview)

### Espresso Testing
Espresso tests (written in Kotlin) are located in 
```android\app\src\androidTest\java\com\augustinus\cgr```

If you don't know how to run Espresso tests, please review the [Android documentation](https://developer.android.com/training/testing/espresso).

Before running them, you will need to add an environment variable to your OS to define the name of your ENVFILE

![](https://i.imgur.com/5Px0uzU.png)