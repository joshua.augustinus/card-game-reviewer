import React from 'react';
import {LogBox} from 'react-native';
import {navigationRef} from '@src/RootNavigation';
import {useDispatch, useSelector} from 'react-redux';
import {updateAppHeaderTitle} from '@src/reducers';
import * as scryfallApi from '@apis/scryfallApi';
import DrawerContainer from '@containers/Drawer/DrawerContainer';
import {RootState} from '@src/types';
import {PaperBottomNav} from '@containers/PaperBottomNav';
import {RnBottomNav} from '@containers/RnBottomNav';
import RNBootSplash from 'react-native-bootsplash';
import {Home} from '@screens/Home';
// ignore specific yellowbox warnings
LogBox.ignoreLogs(['Require cycle:', 'Remote debugger']);

const App = (props: any) => {
  const dispatch = useDispatch();
  const containerType = useSelector((state: RootState) => state.containerType);

  /**
   * Call scryfall once to get Sets
   */
  React.useEffect(() => {
    dispatch(scryfallApi.getSets());
  }, []);

  React.useEffect(() => {
    RNBootSplash.hide(); // immediate
  }, []);

  React.useEffect(() => {
    const unsubscribe = navigationRef.current?.addListener(
      'state',
      (e: any) => {
        const index = e.data?.state?.index;
        if (index >= 0) {
          const currentRoute = e.data.state.routes[index];
          const title = currentRoute.name;
          dispatch(updateAppHeaderTitle(title));
        } else {
          console.log('Navigation event', e);
        }
      },
    );

    return unsubscribe;
  }, []);

  /** Switch between different navigiation types */
  if (containerType === 'PaperBottomNav') return <PaperBottomNav />;
  else if (containerType === 'RnBottomNav') return <RnBottomNav />;
  else if (containerType === 'Drawer') return <DrawerContainer />;
  else return <Home />;
};

export default App;
